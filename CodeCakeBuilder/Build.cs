using Cake.Common.Build;
using Cake.Common.Diagnostics;
using Cake.Common.IO;
using Cake.Common.Solution;
using Cake.Common.Tools.DotNetCore;
using Cake.Common.Tools.DotNetCore.Build;
using Cake.Common.Tools.DotNetCore.Pack;
using Cake.Common.Tools.DotNetCore.Publish;
using Cake.Common.Tools.DotNetCore.Restore;
using Cake.Common.Tools.DotNetCore.Test;
using Cake.Common.Tools.NuGet;
using Cake.Common.Tools.NuGet.Push;
using Cake.Common.Tools.NUnit;
using Cake.Core;
using Cake.Core.Diagnostics;
using Cake.Core.IO;
using CK.Text;
using SimpleGitVersion;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CodeCake
{
    /// <summary>
    /// Standard build "script".
    /// </summary>
    [AddPath( "%UserProfile%/.nuget/packages/**/tools*" )]
    public partial class Build : CodeCakeHost
    {
        public Build()
        {
            Cake.Log.Verbosity = Verbosity.Diagnostic;

            const string solutionName = "CK-Setup";
            const string solutionFileName = solutionName + ".sln";

            var releasesDir = Cake.Directory( "CodeCakeBuilder/Releases" );

            var projects = Cake.ParseSolution( solutionFileName )
                           .Projects
                           .Where( p => !(p is SolutionFolder)
                                        && p.Name != "CodeCakeBuilder" );

            // We do not publish .Tests projects for this solution.
            var projectsToPublish = projects
                                        .Where( p => !p.Path.Segments.Contains( "Tests" ) );

            // The SimpleRepositoryInfo should be computed once and only once.
            SimpleRepositoryInfo gitInfo = Cake.GetSimpleRepositoryInfo();
            // This default global info will be replaced by Check-Repository task.
            // It is allocated here to ease debugging and/or manual work on complex build script.
            CheckRepositoryInfo globalInfo = new CheckRepositoryInfo { Version = gitInfo.SafeNuGetVersion };

            Task( "Check-Repository" )
                .Does( () =>
                {
                    globalInfo = StandardCheckRepository( projectsToPublish, gitInfo );
                    if( globalInfo.ShouldStop )
                    {
                        Cake.TerminateWithSuccess( "All packages from this commit are already available. Build skipped." );
                    }
                } );

            Task( "Clean" )
                .Does( () =>
                 {
                     Cake.CleanDirectories( projects.Select( p => p.Path.GetDirectory().Combine( "bin" ) ) );
                     Cake.CleanDirectories( releasesDir );
                     Cake.DeleteFiles( "Tests/**/TestResult*.xml" );
                 } );

            Task( "Build" )
                .IsDependentOn( "Check-Repository" )
                .IsDependentOn("Clean")
                .Does( () =>
                {
                    StandardSolutionBuild( solutionFileName, gitInfo, globalInfo.BuildConfiguration );
                    // Required to have a published version of the runner that unit tests can use with
                    // the right version.
                    var publishSettings = new DotNetCorePublishSettings().AddVersionArguments( gitInfo, c =>
                    {
                        c.Configuration = globalInfo.BuildConfiguration;
                    } );
                    publishSettings.Framework = "netcoreapp2.0";
                    Cake.DotNetCorePublish( "CKSetup.Runner/CKSetup.Runner.csproj", publishSettings );
                    Cake.DotNetCorePublish( "CKSetup/CKSetup.csproj", publishSettings );
                    publishSettings.Framework = "netcoreapp2.1";
                    Cake.DotNetCorePublish( "CKSetup.Runner/CKSetup.Runner.csproj", publishSettings );
                    Cake.DotNetCorePublish( "CKSetup/CKSetup.csproj", publishSettings );
                } );

            Task( "Unit-Testing" )
                .IsDependentOn( "Build" )
                .WithCriteria( () => Cake.InteractiveMode() == InteractiveMode.NoInteraction
                                     || Cake.ReadInteractiveOption( "RunUnitTests", "Run Unit Tests?", 'Y', 'N' ) == 'Y' )
                .Does( () =>
                {
                    StandardUnitTests( globalInfo.BuildConfiguration, projects.Where( p => p.Name.EndsWith( ".Tests" ) ) );
                } );

            Task( "Push-Runner-To-Remote-Store" )
                .IsDependentOn( "Unit-Testing" )
                .WithCriteria( () => gitInfo.IsValid )
                .Does( () =>
                {
                    var components = new string[]
                    {
                        $"CKSetup.Runner/bin/{globalInfo.BuildConfiguration}/net461",
                        $"CKSetup.Runner/bin/{globalInfo.BuildConfiguration}/netcoreapp2.0/publish",
                        $"CKSetup.Runner/bin/{globalInfo.BuildConfiguration}/netcoreapp2.1/publish",
                        $"CKSetup/bin/{globalInfo.BuildConfiguration}/net461",
                        $"CKSetup/bin/{globalInfo.BuildConfiguration}/netcoreapp2.0/publish",
                        $"CKSetup/bin/{globalInfo.BuildConfiguration}/netcoreapp2.1/publish"
                    };

                    var storeConf = Cake.CKSetupCreateDefaultConfiguration();
                    if( globalInfo.IsLocalCIRelease )
                    {
                        storeConf.TargetStoreUrl = System.IO.Path.Combine( globalInfo.LocalFeedPath, "CKSetupStore" );
                    }
                    if( !storeConf.IsValid )
                    {
                        Cake.Information( "CKSetupStoreConfiguration is invalid. Skipped push to remote store." );
                        return;
                    }
                    
                    Cake.Information( $"Using CKSetupStoreConfiguration: {storeConf}" );

                    if( !Cake.CKSetupAddComponentFoldersToStore( storeConf, components ) )
                    {
                        Cake.TerminateWithError( "Error while registering components in local temporary store." );
                    }
                    if( !Cake.CKSetupPushLocalToRemoteStore( storeConf ) )
                    {
                        Cake.TerminateWithError( "Error while pushing components to remote store." );
                    }
                } );


            Task( "Create-NuGet-Packages" )
                .WithCriteria( () => gitInfo.IsValid )
                .IsDependentOn( "Unit-Testing" )
                .Does( () =>
                {
                    StandardCreateNuGetPackages( releasesDir, projectsToPublish, gitInfo, globalInfo.BuildConfiguration );
                } );

            Task( "Push-NuGet-Packages" )
                .WithCriteria( () => gitInfo.IsValid )
                .IsDependentOn( "Create-NuGet-Packages" )
                .IsDependentOn( "Push-Runner-To-Remote-Store" )
                .Does( () =>
                {
                    StandardPushNuGetPackages( globalInfo, releasesDir );
                } );

            // The Default task for this script can be set here.
            Task( "Default" )
                .IsDependentOn( "Push-NuGet-Packages" );

        }
    }
}
