using Cake.Common.Diagnostics;
using Cake.Common.Tools.DotNetCore;
using Cake.Common.Tools.DotNetCore.Publish;
using Cake.Core;
using CK.Core;
using CK.Text;
using CKSetup;
using CodeCake;
using System;
using System.Collections.Generic;

namespace Cake.Core
{
    /// <summary>
    /// Extends <see cref="ICakeContext"/> with helper related to CKSetup.
    /// </summary>
    public static class CKSetupCakeContextExtensions
    {
        /// <summary>
        /// Name of the environment variable that can defines the Url and optionally the API Key
        /// of the store that will be used to push components.
        /// </summary>
        public const string EnvNameTargetStoreAPIKeyAndUrl = "CKSETUP_CAKE_TARGET_STORE_APIKEY_AND_URL";

        /// <summary>
        /// Name of the environment variable that defines the remote repository url. When not set,
        /// the <see cref="EnvNameTargetStoreAPIKeyAndUrl"/> url is used.
        /// </summary>
        public const string EnvNameRemoteStoreUrl = "CKSETUP_CAKE_REMOTE_STORE_URL";

        /// <summary>
        /// Creates a new <see cref="CKSetupStoreConfiguration"/> object (that can not be <see cref="CKSetupStoreConfiguration.IsValid"/>
        /// and may be mutated).
        /// This configuration is based on environment variables CKSETUP_CAKE_TARGET_STORE_APIKEY_AND_URL
        /// and CKSETUP_CAKE_REMOTE_STORE_URL.
        /// <para>
        /// The CKSETUP_CAKE_TARGET_STORE_APIKEY_AND_URL, if it exists, can contain the API key and target url
        /// (or full local path) separated by a pipe (|) (or a target url alone without any pipe in it).
        /// </para>
        /// <para>
        /// If CKSETUP_REMOTE_STORE_URL does not exist, the target url is used, otherwise
        /// it is left null (and hence will default to <see cref="Facade.DefaultStoreUrl"/>).
        /// </para>
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <returns>A configuration initalized by the environment variables.</returns>
        public static CKSetupStoreConfiguration CKSetupCreateDefaultConfiguration( this ICakeContext @this )
        {
            string targetStoreAPIKeyAndUrl = @this.Environment.GetEnvironmentVariable( EnvNameTargetStoreAPIKeyAndUrl );
            if( targetStoreAPIKeyAndUrl == null && @this.InteractiveMode() != InteractiveMode.NoInteraction )
            {
                targetStoreAPIKeyAndUrl = @this.InteractiveEnvironmentVariable( EnvNameTargetStoreAPIKeyAndUrl );
            }
            string remoteStoreUrl = @this.Environment.GetEnvironmentVariable( EnvNameRemoteStoreUrl );
            return CKSetupStoreConfiguration.CreateFromEnvironmentValues( targetStoreAPIKeyAndUrl, remoteStoreUrl );
        }

        /// <summary>
        /// Registers a set of bin folders (that must have been published if in .net core: their paths
        /// must end with "/publish") into a local store.
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="folders">Sets of folder paths to register.</param>
        /// <param name="storePath">The store path to use. Defaults to <see cref="Facade.DefaultStorePath"/>.</param>
        /// <param name="remoteStoreUrl">
        /// The remote store url. Defaults to <see cref="Facade.DefaultStoreUrl"/>.
        /// The remote store is used to resolve missing embedded components if added components rely
        /// on other components that are not produced locally.
        /// Use "none" to NOT using any remote store.
        /// </param>
        /// <returns>True on success, false if an error occurred.</returns>
        public static bool CKSetupAddComponentFoldersToStore(
            this ICakeContext @this,
            IEnumerable<string> folders,
            string storePath = "CodeCakeBuilder/Releases/TempStore",
            string remoteStoreUrl = null )
        {
            if( folders == null ) throw new ArgumentNullException( nameof( folders ) );
            var monitor = new ActivityMonitor { MinimalFilter = LogFilter.Monitor };
            monitor.Output.RegisterClient( new ActivityMonitorConsoleClient() );
            var binFolders = Facade.ReadBinFolders( monitor, folders );
            if( binFolders == null ) return false;
            var remoteUri = remoteStoreUrl == "none" || remoteStoreUrl == "'none'"
                            ? null
                            : (remoteStoreUrl != null
                                  ? new Uri( remoteStoreUrl, UriKind.Absolute )
                                  : Facade.DefaultStoreUrl);
            using( IRemoteStore remote = remoteUri != null
                                       ? ClientRemoteStore.Create( monitor, remoteUri, null )
                                       : null )
            using( LocalStore store = LocalStore.OpenOrCreate( monitor, storePath ) )
            {
                return store != null && store.CreateLocalImporter( remote ).AddComponent( binFolders ).Import();
            }
        }

        /// <summary>
        /// Registers a set of bin folders (that must have been published if in .net core: their paths
        /// must end with "/publish") into a local store.
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="storeConf">Required configuration.</param>
        /// <param name="folders">Sets of folder paths to register.</param>
        /// <returns>True on success, false if an error occurred.</returns>
        public static bool CKSetupAddComponentFoldersToStore(
            this ICakeContext @this,
            CKSetupStoreConfiguration storeConf,
            IEnumerable<string> folders)
        {
            if( storeConf == null ) throw new ArgumentNullException( nameof( storeConf ) );
            return CKSetupAddComponentFoldersToStore( @this, folders, storeConf.TemporaryStorePath, storeConf.RemoteStoreUrl );
        }


        /// <summary>
        /// Calls <see cref="CKSetupAddComponentFoldersToStore(ICakeContext, IEnumerable{string}, string, string)"/>
        /// after having called Cake.DotNetCorePublish on paths that end with "netcoreapp".
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="binFrameworkPaths">Sets of folder paths to register.</param>
        /// <param name="forcePublish">True to publish even if /publish folder exists.</param>
        /// <param name="storePath">The target store path.</param>
        /// <param name="remoteStoreUrl">
        /// The remote store url. Defaults to <see cref="Facade.DefaultStoreUrl"/>.
        /// The remote store is used to resolve missing embedded components if added components rely
        /// on other components that are not produced locally.
        /// Use "none" to NOT using any remote store.
        /// </param>
        /// <returns>True on success, false if an error occurred.</returns>
        public static bool CKSetupPublishAndAddComponentFoldersToStore(
            this ICakeContext @this,
            IEnumerable<string> binFrameworkPaths,
            bool forcePublish = false,
            string storePath = "CodeCakeBuilder/Releases/TempStore",
            string remoteStoreUrl = null )
        {
            var paths = new List<string>();
            foreach( var p in binFrameworkPaths )
            {
                NormalizedPath pubPath = System.IO.Path.GetFullPath( p );
                var framework = pubPath.LastPart;
                if( framework.StartsWith( "netcoreapp", StringComparison.OrdinalIgnoreCase ) )
                {
                    NormalizedPath publishedPath = pubPath.AppendPart( "publish" );
                    if( forcePublish || !System.IO.Directory.Exists( publishedPath ) )
                    {
                        var pathToConfiguration = pubPath.RemoveLastPart();
                        var configuration = pathToConfiguration.LastPart;
                        var projectPath = pathToConfiguration.RemoveParts( pathToConfiguration.Parts.Count - 2, 2 );
                        var projectName = projectPath.LastPart;
                        @this.Information( $"Publishing {projectName} in {framework}." );
                        @this.DotNetCorePublish( projectPath, new DotNetCorePublishSettings()
                        {
                            // Waiting for NoBuild option: https://github.com/dotnet/cli/issues/5331
                            // /p:NoBuild=True /p:BuildProjectReferences=False do what they can.
                            NoDependencies = true,
                            NoRestore = true,
                            MSBuildSettings = new Common.Tools.DotNetCore.MSBuild.DotNetCoreMSBuildSettings()
                            {
                                ArgumentCustomization = args => args.Append( "/p:NoBuild=True" )
                            },
                            Configuration = configuration,
                            Framework = framework
                        } );
                    }
                    else @this.Information( $"Skipping publish {p} in {framework} since /publish exists." );
                    pubPath = publishedPath;
                }
                paths.Add( pubPath );
            }
            return CKSetupAddComponentFoldersToStore( @this,  paths, storePath, remoteStoreUrl );
        }

        /// <summary>
        /// Calls <see cref="CKSetupAddComponentFoldersToStore(ICakeContext, CKSetupStoreConfiguration, IEnumerable{string})"/>
        /// after having called Cake.DotNetCorePublish on paths that end with "netcoreapp".
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="storeConf">Required <see cref="CKSetupStoreConfiguration"/>.</param>
        /// <param name="binFrameworkPaths">Sets of folder paths to register.</param>
        /// <param name="forcePublish">True to publish even if /publish folder exists.</param>
        /// <returns>True on success, false if an error occurred.</returns>
        public static bool CKSetupPublishAndAddComponentFoldersToStore(
            this ICakeContext @this,
            CKSetupStoreConfiguration storeConf,
            IEnumerable<string> binFrameworkPaths,
            bool forcePublish = false)
        {
            if( storeConf == null ) throw new ArgumentNullException( nameof( storeConf ) );
            return CKSetupPublishAndAddComponentFoldersToStore( @this, binFrameworkPaths, forcePublish, storeConf.TemporaryStorePath, storeConf.RemoteStoreUrl );
        }

        /// <summary>
        /// Pushes the content of a local store path to a remote store.
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="targetStoreUrl">
        /// The target store url or full path to a local store. Must not be null or empty.
        /// Can be <see cref="Facade.DefaultStoreUrl"/> for public components (as long as the API key is knwon).
        /// </param>
        /// <param name="apiKey">The api key that may be required to be able to push components in the target store.</param>
        /// <param name="storePath">The path to the local store to push.</param>
        /// <returns>True on success, false on error.</returns>
        public static bool CKSetupPushLocalToRemoteStore(
            this ICakeContext @this,
            string targetStoreUrl,
            string apiKey,
            string storePath = "CodeCakeBuilder/Releases/TempStore" )
        {
            if( String.IsNullOrWhiteSpace( targetStoreUrl ) ) throw new ArgumentNullException( nameof( targetStoreUrl ) );
            var monitor = new ActivityMonitor { MinimalFilter = LogFilter.Monitor };
            monitor.Output.RegisterClient( new ActivityMonitorConsoleClient() );
            using( LocalStore store = LocalStore.OpenOrCreate( monitor, storePath ) )
            {
                return store != null
                        && store.PushComponents( comp => true, new Uri( targetStoreUrl, UriKind.Absolute ), apiKey );
            }
        }

        /// <summary>
        /// Pushes the content of a local store path to a remote store.
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="storeConf">Required <see cref="CKSetupStoreConfiguration"/>.</param>
        /// <returns>True on success, false on error.</returns>
        public static bool CKSetupPushLocalToRemoteStore(
            this ICakeContext @this,
            CKSetupStoreConfiguration storeConf )
        {
            if( storeConf == null ) throw new ArgumentNullException( nameof( storeConf ) );
            return CKSetupPushLocalToRemoteStore( @this, storeConf.TargetStoreUrl, storeConf.TargetStoreAPIKey, storeConf.TemporaryStorePath );
        }

    }
}
