using System;
using System.Collections.Generic;
using System.Text;

namespace CK.Testing
{
    /// <summary>
    /// Defines the store type: can be a zip file or a directory.
    /// </summary>
    public enum CKSetupStoreType
    {
        /// <summary>
        /// The store is a directory.
        /// </summary>
        Directory,

        /// <summary>
        /// The store is a zip file.
        /// </summary>
        Zip
    }
}
