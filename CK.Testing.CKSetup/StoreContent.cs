using CK.Core;
using CK.Text;
using CKSetup;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CK.Testing.CKSetup
{
    /// <summary>
    /// Helper class that ease working with <see cref="ComponentDB"/>.
    /// </summary>
    public class StoreContent
    {
        /// <summary>
        /// The Xml raw database.
        /// </summary>
        public readonly XElement Db;

        /// <summary>
        /// The Xml database without varying parts: all Version and PrototypeStoreUrl attributes are removed
        /// </summary>
        public readonly XElement DbWithoutVaryingParts;

        /// <summary>
        /// The immutable ComponentDB itself.
        /// </summary>
        public readonly ComponentDB ComponentDB;

        /// <summary>
        /// Exposes the different <see cref="ComponentFile"/> that shares the same content SHA1.
        /// </summary>
        public struct FileEntry : IEquatable<FileEntry>
        {
            /// <summary>
            /// Initializes a new <see cref="FileEntry"/>.
            /// </summary>
            /// <param name="db">The owning database.</param>
            /// <param name="fullName">The file name.</param>
            public FileEntry( ComponentDB db, string fullName )
            {
                string sha = RemoveCompressionPrefix( fullName );
                SHA1Value v = SHA1Value.Parse( sha );
                ComponentFiles = db.Components.SelectMany( c => c.Files ).Where( f => f.SHA1 == v );
                SHA1 = v;
                ComponentFiles.Should().NotBeNullOrEmpty();
            }

            /// <summary>
            /// Gets the file SHA1.
            /// </summary>
            public SHA1Value SHA1 { get; }

            /// <summary>
            /// Gets the <see cref="ComponentFile"/> from this <see cref="ComponentDB"/> that are actually the same.
            /// </summary>
            public IEnumerable<ComponentFile> ComponentFiles { get; }

            /// <summary>
            /// Defines equality based on this <see cref="SHA1"/> and all <see cref="ComponentFiles"/>' SHA1.
            /// </summary>
            /// <param name="other">The other FileEntry. Must not be null.</param>
            /// <returns>True if both have the same content.</returns>
            public bool Equals( FileEntry other )
            {
                if( SHA1 != other.SHA1 ) return false;
                return ComponentFiles.SequenceEqual( other.ComponentFiles );
            }

            /// <summary>
            /// Overridden to call <see cref="Equals(FileEntry)"/>.
            /// </summary>
            /// <param name="obj">Other object.</param>
            /// <returns>True if obj is a <see cref="FileEntry"/> that has the same content as this one.</returns>
            public override bool Equals( object obj ) => obj is FileEntry e ? Equals( e ) : false;

            /// <summary>
            /// Gets a hash based on the <see cref="SHA1"/>.
            /// </summary>
            /// <returns>The hash code.</returns>
            public override int GetHashCode() => SHA1.GetHashCode();

            /// <summary>
            /// Overridden to return the list of <see cref="ComponentFiles"/>.
            /// </summary>
            /// <returns>Comma separated</returns>
            public override string ToString() => ComponentFiles.Select( c => c.ToString() ).Concatenate();
        }

        /// <summary>
        /// Gets all the <see cref="FileEntries"/>.
        /// </summary>
        public readonly Dictionary<SHA1Value,FileEntry> FileEntries;

        /// <summary>
        /// Checks that this <see cref="StoreContent"/> is the same as anotther one.
        /// </summary>
        /// <param name="other">Other store to challenge.</param>
        public void ShouldBeEquivalentTo( StoreContent other )
        {
            DbWithoutVaryingParts.ToString(SaveOptions.DisableFormatting)
                .Should().Be( other.DbWithoutVaryingParts.ToString( SaveOptions.DisableFormatting ) );
            FileEntries.Should().BeEquivalentTo( other.FileEntries );
        }

        /// <summary>
        /// Checks that all files of a component exist.
        /// </summary>
        /// <param name="c">The component whose files must be stored.</param>
        public void AllComponentFilesShouldBeStored( Component c )
        {
            c.Files.All( f => FileEntries.ContainsKey( f.SHA1 ) ).Should().BeTrue();
        }

        /// <summary>
        /// Check that no file of a component exist.
        /// </summary>
        /// <param name="c">The component whose files must not be stored.</param>
        public void NoComponentFilesShouldBeStored( Component c )
        {
            c.Files.Any( f => FileEntries.ContainsKey( f.SHA1 ) ).Should().BeFalse();
        }

        /// <summary>
        /// Checks that a component contains some files.
        /// </summary>
        /// <param name="c">The component.</param>
        /// <param name="file">The expected files.</param>
        public void ComponentShouldContainFiles( Component c, params string[] file )
        {
            c.Files.Select( f => f.Name ).Should().Contain( file );
        }

        /// <summary>
        /// Checks that a component does not contain some files.
        /// </summary>
        /// <param name="c">The component.</param>
        /// <param name="file">The unexpected files.</param>
        public void ComponentShouldNotContainFiles( Component c, params string[] file )
        {
            c.Files.Select( f => f.Name ).Should().NotContain( file );
        }

        /// <summary>
        /// Initializes a new <see cref="StoreContent"/>.
        /// </summary>
        /// <param name="path">The path to the store.</param>
        public StoreContent( string path )
        {
            if( path.EndsWith(".zip", StringComparison.OrdinalIgnoreCase))
            {
                using( var z = ZipFile.Open( path, ZipArchiveMode.Read ) )
                {
                    var e = z.GetEntry( "None/" + LocalStore.DbXmlFileName );
                    e.Should().NotBeNull();
                    using( var content = e.Open() )
                    {
                        Db = NormalizeWithoutAnyOrder( XDocument.Load( content ).Root );
                    }
                    ComponentDB = new ComponentDB( Db );
                    FileEntries = z.Entries
                                .Where( x => RemoveCompressionPrefix( x.FullName ) != LocalStore.DbXmlFileName )
                                .Select( x => new FileEntry( ComponentDB, x.FullName ) )
                                .ToDictionary( x => x.ComponentFiles.First().SHA1 );
                }
            }
            else
            {
                path = Path.GetFullPath( path ) + FileUtil.DirectorySeparatorString;
                string pathNone = FileUtil.NormalizePathSeparator( Path.Combine( path, "None" ), true );
                string pathGZiped = FileUtil.NormalizePathSeparator( Path.Combine( path, "GZiped" ), true );
                File.Exists( pathNone + LocalStore.DbXmlFileName ).Should().BeTrue();
                Db = NormalizeWithoutAnyOrder( XDocument.Load( pathNone + LocalStore.DbXmlFileName ).Root );
                ComponentDB = new ComponentDB( Db );
                FileEntries = Directory.EnumerateFiles( pathNone, "*", SearchOption.AllDirectories )
                                .Concat( Directory.EnumerateFiles( pathGZiped, "*", SearchOption.AllDirectories ) )
                                .Select( p => p.Substring( path.Length ) )
                                .Where( x => RemoveCompressionPrefix( x ) != LocalStore.DbXmlFileName )
                                .Select( x => new FileEntry( ComponentDB, x ) )
                                .ToDictionary( x => x.ComponentFiles.First().SHA1 );
            }
            DbWithoutVaryingParts = RemoveVaryingParts( Db );
        }

        XElement RemoveVaryingParts( XElement e )
        {
            return new XElement(
                        e.Name,
                        e.Attributes().Where( a => a.Name != "Version" && a.Name != "PrototypeStoreUrl" ).Select( a => new XAttribute(a) ),
                        e.Elements().Select( c => RemoveVaryingParts( c ) ) );
        }

        static string RemoveCompressionPrefix( string name )
        {
            if( name.StartsWith( "None/" ) || name.StartsWith( @"None\" ) ) return name.Substring( 5 );
            if( name.StartsWith( "GZiped/" ) || name.StartsWith( @"GZiped\" ) ) return name.Substring( 7 );
            throw new Exception( $@"File name {name} should start with 'None/', 'None\', 'GZiped/' or 'GZiped\'." );
        }

        static XElement NormalizeWithoutAnyOrder( XElement element )
        {
            if( element.HasElements )
            {
                return new XElement(
                    element.Name,
                    element.Attributes().OrderBy( a => a.Name.ToString() ),
                    element.Elements()
                        .OrderBy( a => a.Name.ToString() )
                        .Select( e => NormalizeWithoutAnyOrder( e ) )
                        .OrderBy( e => e.Attributes().Count() )
                        .OrderBy( e => e.Attributes()
                                        .Select( a => a.Value )
                                        .Concatenate( "\u0001" ) )
                        .ThenBy( e => e.Value ) );
            }
            if( element.IsEmpty || string.IsNullOrEmpty( element.Value ) )
            {
                return new XElement( element.Name,
                                     element.Attributes()
                                            .OrderBy( a => a.Name.ToString() ) );
            }
            return new XElement( element.Name,
                                 element.Attributes()
                                        .OrderBy( a => a.Name.ToString() ),
                                 element.Value );
        }
    }
}
