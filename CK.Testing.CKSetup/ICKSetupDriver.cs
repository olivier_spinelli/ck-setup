using CK.Text;
using CKSetup;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace CK.Testing.CKSetup
{
    /// <summary>
    /// Exposes an API to manipulate CKSetup.
    /// </summary>
    public interface ICKSetupDriver
    {

        /// <summary>
        /// Gets the semi colon (;) separated paths from "CKSetup/AutoRegisterLocalComponentPaths" configuration.
        /// <para>
        /// When this is not empty, <see cref="GenerateComponentsMode"/> is necessarily true and these componnents
        /// are published (if in NetCoreApp, see <see cref="PublishAndAddComponentFoldersToStore"/>) and added
        /// to the <see cref="DefaultStorePath"/> the first time it is used.
        /// </para>
        /// <para>
        /// Note that before adding the components, any components with a <see cref="CSemVer.SVersion.ZeroVersion"/> version
        /// are removed from the <see cref="DefaultStorePath"/>.
        /// </para>
        /// </summary>
        IReadOnlyList<NormalizedPath> AutoRegisterLocalComponentPaths { get; }

        /// <summary>
        /// Gets the "CKSetup/GenerateComponentsMode" configuration that is false by default or true
        /// if <see cref="AutoRegisterLocalComponentPaths"/> is not empty.
        /// <para>
        /// For projects that generate components (runtime or engines) this should be set to true.
        /// </para>
        /// </summary>
        bool GenerateComponentsMode { get; }

        /// <summary>
        /// Gets the default store path from "CKSetup/DefaultStorePath" configuration.
        /// <para>
        /// For projects that do not generate components (<see cref="GenerateComponentsMode"/> is false),
        /// this defaults to <see cref="Facade.DefaultStorePath"/> (The Environment.SpecialFolder.LocalApplicationData/CKSetupStore).
        /// </para>
        /// <para>
        /// When GenerateComponentsMode is true, this defaults to "<see cref="LocalStoresFolder"/>/Default" directory:
        /// the store is NOT the shared one.
        /// </para>
        /// </summary>
        NormalizedPath DefaultStorePath { get; }

        /// <summary>
        /// Gets or sets the working directory that the runner will use when <see cref="SetupConfiguration.WorkingDirectory"/>
        /// is null.
        /// <para>
        /// Defaults to "CKSetup/WorkingDirectory" configuration or an empty path (<see cref="NormalizedPath.IsEmpty"/>).
        /// If this path is empty, a temporary folder is created (<see cref="System.IO.Path.GetTempPath"/>). 
        /// A unique timed folder (see <see cref="CK.Core.FileUtil.CreateUniqueTimedFolder"/>) is created for
        /// each setup and let as-is on the file system.
        /// </para>
        /// </summary>
        NormalizedPath WorkingDirectory { get; set; }

        /// <summary>
        /// Gets the semi colon (;) separated paths from "CKSetup/DefaultBinPaths" configuration.
        /// <para>
        /// This is used to fill the <see cref="SetupConfiguration.BinPaths"/> when <see cref="Run"/>
        /// is called if (and only if) the configuration parameter's BinPaths is empty.
        /// </para>
        /// <para>
        /// When not rooted (relative paths), the base directory is
        /// the <see cref="IBasicTestHelper.TestProjectFolder"/> (same behavior as <see cref="DefaultStorePath"/> and <see cref="WorkingDirectory"/>).
        /// </para>
        /// <para>
        /// BinPaths can use standard placeholders from CK.Testing like {BinFolder}, {BuildConfiguration} but also
        /// the special {CKSetupAutoTargetProjectBinFolder}. This placeholder will try to find the associated SUT
        /// project from the <see cref="IBasicTestHelper.TestProjectName"/> by removing its ".Tests" suffix (and optionally
        /// the ".NetCore.Tests" suffix) and looking for a named project above.
        /// When running on the .NetFramework, the path is considered to be {SUTProject}/bin/{BuildConfiguration}/net461 otherwise
        /// it is {SUTProject}/bin/{BuildConfiguration}/netstandard2.0.
        /// </para>
        /// <para>
        /// To change this default behavior one can use a trailing double dots .. path: {CKSetupAutoTargetProjectBinFolder}/../netcoreapp2.0/publish
        /// will target the {SUTProject}/bin/{BuildConfiguration}/netcoreapp2.0/publish folder. (Note that any paths that may appear
        /// before the {CKSetupAutoTargetProjectBinFolder} are simply ignored.)
        /// </para>
        /// <para>
        /// When a bin path ends with "/publish", dotnet publish is automatically called on the parent path (see <see cref="DotNetPublish(string, bool)"/>).
        /// </para>
        /// <para>
        /// If no paths at all are specified, Run ultimately uses the <see cref="IBasicTestHelper.BinFolder"/>.
        /// </para>
        /// </summary>
        IReadOnlyList<NormalizedPath> DefaultBinPaths { get; }

        /// <summary>
        /// Gets the semi colon (;) separated <see cref="SetupDependency"/> from "CKSetup/DefaultDependencies" configuration.
        /// A SetupDependency is the name and an optional minimal version like "CKSetup.Runner/6.0.0-a00-00-develop-0032".
        /// <para>
        /// This is used to fill the <see cref="SetupConfiguration.Dependencies"/> when <see cref="Run"/>
        /// is called if (and only if) the Dependencies is empty.
        /// </para>
        /// </summary>
        IReadOnlyList<SetupDependency> DefaultDependencies { get; }

        /// <summary>
        /// Gets the default store remote url from "CKSetup/DefaultStoreUrl" configuration.
        /// Defaults to <see cref="Facade.DefaultStoreUrl"/>.
        /// Can be set to a local zip or directory path (useful when no internet connection is
        /// available and a local store already contains all needed components).
        /// </summary>
        Uri DefaultStoreUrl { get; }

        /// <summary>
        /// Gets or sets whether the debugger should be launched by the runner process.
        /// Defaults to "CKSetup/DefaultLaunchDebug" or "CKSetup/LaunchDebug" configurations or false
        /// if no configuration exist.
        /// </summary>
        bool DefaultLaunchDebug { get; set; }

        /// <summary>
        /// Gets or sets whether the setup should be ran, regardless of the signature files that may
        /// exist in the <see cref="SetupConfiguration.BinPaths"/> folders.
        /// Defaults to "CKSetup/DefaultForceSetup" or "CKSetup/ForceSetup" configurations or false
        /// if no configuration exist.
        /// </summary>
        bool DefaultForceSetup { get; set; }

        /// <summary>
        /// Gets the semi colon (;) separated preferred target runtimes from "CKSetup/DefaultPreferredTargetRuntimes"
        /// configuration.
        /// <para>
        /// This is used to fill the <see cref="SetupConfiguration.PreferredTargetRuntimes"/> when <see cref="Run"/>
        /// is called if (and only if) the configuration parameter's PreferredTargetRuntimes is empty.
        /// </para>
        /// </summary>
        IReadOnlyList<TargetRuntime> DefaultPreferredTargetRuntimes { get; }

        /// <summary>
        /// This event fires once and only once before a store is used by <see cref="Run"/>.
        /// This can be used to populate the store with components as needed.
        /// Note that this event is not raised when the <see cref="SetupRunningEventArgs.StorePath"/>
        /// is the shared <see cref="Facade.DefaultStorePath"/>.
        /// </summary>
        event EventHandler<StorePathInitializationEventArgs> InitializeStorePath;

        /// <summary>
        /// Fired before <see cref="Run"/>. This can be used to cancel the setup 
        /// This can be used to populate the store with components as needed.
        /// </summary>
        event EventHandler<SetupRunningEventArgs> SetupRunning;

        /// <summary>
        /// Fired after <see cref="Run"/>.
        /// </summary>
        event EventHandler<SetupRanEventArgs> SetupRan;

        /// <summary>
        /// Runs a configuration.
        /// </summary>
        /// <param name="configuration">The configuration to run. Must not be null.</param>
        /// <param name="storePath">The store path to use. Defaults to <see cref="DefaultStorePath"/>.</param>
        /// <param name="remoteStoreUrl">
        /// The remote store url. Defaults to <see cref="DefaultStoreUrl"/>.
        /// Use "none" to explicitly not sollicitating any remote store.
        /// </param>
        /// <param name="launchDebug">
        /// True to call <see cref="System.Diagnostics.Debugger.Launch"/> at the very start of the runner process.
        /// Defaults to <see cref="DefaultLaunchDebug"/>.
        /// </param>
        /// <param name="forceSetup">
        /// True to run the setup, regardless of the signature files that may exist in
        /// the <see cref="SetupConfiguration.BinPaths"/> folders.
        /// Defaults to <see cref="DefaultForceSetup"/>.
        /// </param>
        /// <returns>Run result.</returns>
        CKSetupRunResult Run( SetupConfiguration configuration, string storePath = null, string remoteStoreUrl = null, bool? launchDebug = null, bool? forceSetup = null );

        /// <summary>
        /// Path to a folder for local stores: "<see cref="IBasicTestHelper.TestProjectFolder"/>/LocalStores".
        /// This folder is globally cleared once (the first time this helper is used).
        /// </summary>
        NormalizedPath LocalStoresFolder { get; }

        /// <summary>
        /// Gets a path to a local store in <see cref="LocalStoresFolder"/>, either as a zip file or a directory.
        /// Use <see cref="GetCleanTestStorePath"/> to clear it before returning it.
        /// </summary>
        /// <param name="type">Type of the store.</param>
        /// <param name="suffix">Optional suffix (will appear before the .zip extension or at the end of the folder).</param>
        /// <param name="name">Name (automatically sets from the caller method name).</param>
        /// <returns>Path to a zip file or a directory that may already exists.</returns>
        NormalizedPath GetTestStorePath( CKSetupStoreType type, string suffix = null, [CallerMemberName]string name = null );

        /// <summary>
        /// Gets a path to a local store in <see cref="LocalStoresFolder"/>, either as a zip file or a directory.
        /// If the store exists it is deleted.
        /// </summary>
        /// <param name="type">Type of the store.</param>
        /// <param name="suffix">Optional suffix (will appear before the .zip extension or at the end of the folder).</param>
        /// <param name="name">Name (automatically sets from the caller method name).</param>
        /// <returns>Path to a zip file or directory that does not exist.</returns>
        NormalizedPath GetCleanTestStorePath( CKSetupStoreType type, string suffix = null, [CallerMemberName]string name = null );

        /// <summary>
        /// Runs "dotnet publish" on an output bin folder.
        /// The configuration (Debug/Release) and target framework (netcoreapp2.0 for instance) are extracted
        /// from the path. This must not be called on .Net framework (like 'net461' folders) and on net standard
        /// targets (like 'netstandard2.0').
        /// </summary>
        /// <param name="pathToFramework">Path to the framework (typically ends with "bin/Debug/netcoreapp2.0").</param>
        /// <param name="skipExistingPublishFolder">
        /// True to skip the publication if the "<paramref name="pathToFramework"/>/publish"
        /// folder exists.
        /// </param>
        /// <returns>The full path to the published folder (ends with "/publish") or null on error.</returns>
        string DotNetPublish( string pathToFramework, bool skipExistingPublishFolder = false );

        /// <summary>
        /// Registers a set of bin folders (that must have been published if in .net core: their paths
        /// must end with "/publish") into a local store.
        /// The <paramref name="storePath"/> can not be the shared <see cref="Facade.DefaultStorePath"/> otherwise
        /// an <see cref="ArgumentException"/> is thrown.
        /// </summary>
        /// <param name="publishedFolders">Sets of folder paths to register.</param>
        /// <param name="storePath">The target store path. Defaults to <see cref="DefaultStorePath"/>.</param>
        /// <param name="remoteStoreUrl">
        /// The remote store url. This remote is used to resolve any embedded components that may be found in
        /// <paramref name="publishedFolders"/> and that are not already in the local store.
        /// Defaults to <see cref="DefaultStoreUrl"/>.
        /// Use "none" to explicitly not sollicitating any remote store.
        /// </param>
        /// <returns>True on success, false if an error occurred.</returns>
        bool AddComponentFoldersToStore( IEnumerable<string> publishedFolders, string storePath = null, string remoteStoreUrl = null );

        /// <summary>
        /// Removes components that match a predicate.
        /// This does not remove any potential dependencies (a <see cref="ComponentDependency"/>
        /// only defines the name of its target and a minimal version).
        /// This is typically used from unit tests to remove the <see cref="CSemVer.SVersion.ZeroVersion"/>
        /// version of components.
        /// The <paramref name="storePath"/> can not be the shared <see cref="Facade.DefaultStorePath"/> otherwise
        /// an <see cref="ArgumentException"/> is thrown.
        /// </summary>
        /// <param name="exclude">A predicate that must return true to remove the component.</param>
        /// <param name="garbageCollectFiles">True to delete all stored files that are no more referenced by any component.</param>
        /// <param name="storePath">The target store path. Defaults to <see cref="DefaultStorePath"/>.</param>
        /// <returns>The number of removed components.</returns>
        int RemoveComponentsFromStore( Func<Component, bool> exclude, bool garbageCollectFiles = false, string storePath = null );

        /// <summary>
        /// Calls <see cref="AddComponentFoldersToStore"/> after having called <see cref="DotNetPublish"/> on
        /// paths that end "netcoreapp".
        /// The <paramref name="storePath"/> can not be the shared <see cref="Facade.DefaultStorePath"/> otherwise
        /// an <see cref="ArgumentException"/> is thrown.
        /// </summary>
        /// <param name="binFrameworkPaths">Sets of folder paths to register.</param>
        /// <param name="skipExistingPublishFolder">
        /// True to skip the publication in .net core if "publish/" frolder already exists.
        /// See <see cref="DotNetPublish(string, bool)"/>.
        /// </param>
        /// <param name="storePath">The target store path. Defaults to <see cref="DefaultStorePath"/>.</param>
        /// <param name="remoteStoreUrl">
        /// The remote store url. This remote is used to resolve any embedded components that may be found in
        /// <paramref name="binFrameworkPaths"/> and that are not already in the local store.
        /// Defaults to <see cref="DefaultStoreUrl"/>.
        /// Use "none" to explicitly not sollicitating any remote store.
        /// </param>
        /// <returns>True on success, false if an error occurred.</returns>
        bool PublishAndAddComponentFoldersToStore( IEnumerable<string> binFrameworkPaths, bool skipExistingPublishFolder = false, string storePath = null, string remoteStoreUrl = null );

        /// <summary>
        /// Pushes the content of a local store path to a remote store.
        /// <see cref="GenerateComponentsMode"/> must be true otherwise an <see cref="InvalidOperationException"/> is thrown.
        /// </summary>
        /// <param name="storePath">The path to the local store to push. Must not be null.</param>
        /// <param name="remoteStoreUrl">The remote store url. Must not be null.</param>
        /// <param name="apiKey">The api key that may be required to be able to push components in the remote store.</param>
        /// <returns>True on success, false on error.</returns>
        bool PushLocalStoreToRemote( string storePath, string remoteStoreUrl, string apiKey );
    }
}
