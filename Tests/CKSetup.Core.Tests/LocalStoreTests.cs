using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK.Core;
using CKSetup.StreamStore;
using System.IO;
using FluentAssertions;
using static CK.Testing.CKSetupTestHelper;
using CK.Testing;
using CK.Testing.CKSetup;
using System.Xml.Linq;
using CK.Text;
using CSemVer;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class LocalStoreTests
    {
        static string RegisterInStoreWithAllComponents( CKSetupStoreType sourceType )
        {
            string sourcePath = TestHelper.CKSetup.GetTestStorePath( sourceType, null, "StoreWithAllComponents" );
            TestHelper.OnlyOnce( () =>
            {
                TestHelper.CKSetup.RemoveComponentsFromStore( c => true, true, sourcePath );
                LocalHelper.RegisterAllComponents( sourcePath );

            }, $"push_to_StoreWithAllComponents_local_remote:{sourceType}" );
            return sourcePath;
        }

        [TestCase( CKSetupStoreType.Zip, CKSetupStoreType.Zip )]
        [TestCase( CKSetupStoreType.Directory, CKSetupStoreType.Directory )]
        [TestCase( CKSetupStoreType.Zip, CKSetupStoreType.Directory )]
        [TestCase( CKSetupStoreType.Directory, CKSetupStoreType.Zip )]
        public void push_to_a_local_remote( CKSetupStoreType sourceType, CKSetupStoreType targetType )
        {
            string sourcePath = RegisterInStoreWithAllComponents( sourceType );

            string targetPath = TestHelper.CKSetup.GetTestStorePath( targetType, "-From-" + sourceType );
            // Ensures that the target store is created.
            LocalStore.OpenOrCreate( TestHelper.Monitor, targetPath ).Dispose();

            using( var source = LocalStore.Open( TestHelper.Monitor, sourcePath ) )
            {
                var targetUri = new Uri( targetPath );
                source.PushComponents( c => true, targetUri, null ).Should().BeTrue();
            }
            var sourceContent = new StoreContent( sourcePath );
            var targetContent = new StoreContent( targetPath );
            sourceContent.ShouldBeEquivalentTo( targetContent );
        }

        [Test]
        public void running_with_missing_dependencies_updates_the_local_store()
        {
            string remotePath = RegisterInStoreWithAllComponents( CKSetupStoreType.Directory );
            string localStore = TestHelper.CKSetup.GetCleanTestStorePath( CKSetupStoreType.Directory );

            var conf = new SetupConfiguration();
            conf.BinPaths.Add( LocalHelper.StupidModelNetStandard20 );
            conf.Configuration = new XElement( "StupidConf" );
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            TestHelper.CKSetup.Run( conf, localStore, remotePath, forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );

            // Our local store has the shared Facade.DefaultStorePath as its prototype.
            // When runnig on a dirty, non commited repository, the CKSetup.Runner/0.0.0-0 may have been
            // resolved by the shared store to an existing release version (greater than the zero version),
            // this why 2 <= localContent.ComponentDB.Components.Count <= 3.
            var localContent = new StoreContent( localStore );
            localContent.ComponentDB.Components.Count.Should().BeInRange( 2, 3 );

            TestHelper.CKSetup.Run( conf, localStore, "none", forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );
        }

        [TestCase( CKSetupStoreType.Directory )]
        [TestCase( CKSetupStoreType.Zip )]
        public void running_with_prototype_store_updates_the_local_store( CKSetupStoreType type )
        {
            string remotePath = RegisterInStoreWithAllComponents( CKSetupStoreType.Directory );
            string localStore = TestHelper.CKSetup.GetCleanTestStorePath( type );
            using( var s = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore ) )
            {
                s.PrototypeStoreUrl = new Uri( remotePath, UriKind.Absolute );
            }

            var conf = new SetupConfiguration();
            conf.BinPaths.Add( LocalHelper.StupidModelNetStandard20 );
            conf.Configuration = new XElement( "StupidConf" );
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            TestHelper.CKSetup.Run( conf, localStore, "none", forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );

            var localContent = new StoreContent( localStore );
            localContent.ComponentDB.Components.Should().HaveCount( 2 );

            TestHelper.CKSetup.Run( conf, localStore, "none", forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );
        }

        [Test]
        public void chaining_stores_with_prototype()
        {
            bool StoreHasItsComponents( string path )
            {
                var localContent = new StoreContent( path );
                return localContent.ComponentDB.Components.Count == 2;
            }

            string remotePath = RegisterInStoreWithAllComponents( CKSetupStoreType.Directory );
            string localStore1 = TestHelper.CKSetup.GetCleanTestStorePath( CKSetupStoreType.Directory, "Level1" );
            string localStore2 = TestHelper.CKSetup.GetCleanTestStorePath( CKSetupStoreType.Directory, "Level2" );
            string localStore3 = TestHelper.CKSetup.GetCleanTestStorePath( CKSetupStoreType.Directory, "Level3" );
            using( var s = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore1 ) )
            {
                s.PrototypeStoreUrl = new Uri( remotePath, UriKind.Absolute );
            }
            using( var s = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore2 ) )
            {
                s.PrototypeStoreUrl = new Uri( localStore1, UriKind.Absolute );
            }
            using( var s = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore3 ) )
            {
                s.PrototypeStoreUrl = new Uri( localStore2, UriKind.Absolute );
            }
            StoreHasItsComponents( localStore1 ).Should().BeFalse();
            StoreHasItsComponents( localStore2 ).Should().BeFalse();
            StoreHasItsComponents( localStore3 ).Should().BeFalse();

            var conf = new SetupConfiguration();
            conf.BinPaths.Add( LocalHelper.StupidModelNetStandard20 );
            conf.Configuration = new XElement( "StupidConf" );
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            TestHelper.CKSetup.Run( conf, localStore3, "none", forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );

            StoreHasItsComponents( localStore1 ).Should().BeTrue();
            StoreHasItsComponents( localStore2 ).Should().BeTrue();
            StoreHasItsComponents( localStore3 ).Should().BeTrue();
        }

        [Test]
        public void importing_absolutely_no_components_does_not_throw()
        {
            NormalizedPath remote = TestHelper.CKSetup.GetTestStorePath( CKSetupStoreType.Directory, "Remote" );
            NormalizedPath local = TestHelper.CKSetup.GetTestStorePath( CKSetupStoreType.Directory, "Local" );

            using( var rem = LocalStore.OpenOrCreate( TestHelper.Monitor, remote ) )
            using( var loc = LocalStore.OpenOrCreate( TestHelper.Monitor, local ) )
            {
                var unexisting = new[] { new ComponentRef( "Does.Not.Exist", TargetFramework.Net461, SVersion.ZeroVersion ) };
                ComponentMissingDescription missing = new ComponentMissingDescription( unexisting );
                using( var mem = new MemoryStream() )
                {
                    rem.ExportComponents( missing, mem, TestHelper.Monitor );
                    mem.Position = 0;
                    loc.ImportComponents( mem, rem.Remote ).Should().BeTrue();
                }
            }
        }

        class FakeDownloader : IComponentFileDownloader
        {
            public Uri Url => new Uri( "https://fake.fake" );

            public StoredStream GetDownloadStream( IActivityMonitor monitor, SHA1Value file, CompressionKind kind )
            {
                throw new NotImplementedException();
            }
        }

        [Test]
        public void importing_from_an_empty_or_invalid_stream_does_not_throw_but_is_an_error()
        {
            NormalizedPath storePath = TestHelper.CKSetup.GetTestStorePath( CKSetupStoreType.Directory );

            using( var store = LocalStore.OpenOrCreate( TestHelper.Monitor, storePath ) )
            {
                using( var mem = new MemoryStream() )
                {
                    store.ImportComponents( mem, new FakeDownloader() ).Should().BeFalse();
                }
            }
        }


        [Test]
        public void mapping_CKSetupAutoTargetProjectBinFolder_in_BinPaths()
        {
            SetupConfiguration conf = new SetupConfiguration();
            conf.BinPaths.Add( "{CKSetupAutoTargetProjectBinFolder}" );
            conf.BinPaths.Add( $"{TestHelper.BinFolder}/{{CKSetupAutoTargetProjectBinFolder}}" );
            conf.BinPaths.Add( $"ThisIsIgnored/{{CKSetupAutoTargetProjectBinFolder}}/../../.." );
            conf.BinPaths.Add( $"{{CKSetupAutoTargetProjectBinFolder}}/../netcoreapp2.0/publish" );
            EventHandler<SetupRunningEventArgs> hook = ( o, e ) =>
             {
                 e.Configuration.BinPaths.Should().HaveCount( 4 );
                 e.Configuration.BinPaths[0].Should().StartWith( $"{TestHelper.RepositoryFolder}\\CKSetup.Core\\bin\\{TestHelper.BuildConfiguration}" );
                 e.Configuration.BinPaths[1].Should().StartWith( $"{TestHelper.RepositoryFolder}\\CKSetup.Core\\bin\\{TestHelper.BuildConfiguration}" );
                 e.Configuration.BinPaths[2].Should().Be( $"{TestHelper.RepositoryFolder}\\CKSetup.Core" );
                 var toNetCore20Published = e.Configuration.BinPaths[3];
                 toNetCore20Published.Should().Be( $"{TestHelper.RepositoryFolder}\\CKSetup.Core\\bin\\{TestHelper.BuildConfiguration}\\netcoreapp2.0\\publish" );
                 e.Cancel = true;
             };
            TestHelper.CKSetup.SetupRunning += hook;
            try
            {
                TestHelper.CKSetup.Run( conf ).Should().Be( CKSetupRunResult.None );
            }
            finally
            {
                TestHelper.CKSetup.SetupRunning -= hook;
            }
        }


    }
}
