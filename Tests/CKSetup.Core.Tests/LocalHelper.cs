using CK.Text;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    static class LocalHelper
    {
        static public NormalizedPath StupidModelNetStandard20 = TestHelper.SolutionFolder.Combine(
                                        $"Tests/StupidModel/bin/{TestHelper.BuildConfiguration}/netstandard2.0" );
        static public NormalizedPath StupidModelNet461 = TestHelper.SolutionFolder.Combine(
                                        $"Tests/StupidModel/bin/{TestHelper.BuildConfiguration}/net461" );

        static public NormalizedPath StupidEngineNetCoreApp20 = TestHelper.SolutionFolder.Combine(
                                        $"Tests/StupidEngine/bin/{TestHelper.BuildConfiguration}/netcoreapp2.0" );
        static public NormalizedPath StupidEngineNetCoreApp21 = TestHelper.SolutionFolder.Combine(
                                        $"Tests/StupidEngine/bin/{TestHelper.BuildConfiguration}/netcoreapp2.1" );
        static public NormalizedPath StupidEngineNet461 = TestHelper.SolutionFolder.Combine(
                                        $"Tests/StupidEngine/bin/{TestHelper.BuildConfiguration}/net461" );

        static public NormalizedPath RunnerNetCoreApp20 = TestHelper.SolutionFolder.Combine(
                                        $"CKSetup.Runner/bin/{TestHelper.BuildConfiguration}/netcoreapp2.0" );
        static public NormalizedPath RunnerNetCoreApp21 = TestHelper.SolutionFolder.Combine(
                                        $"CKSetup.Runner/bin/{TestHelper.BuildConfiguration}/netcoreapp2.1" );
        static public NormalizedPath RunnerNet461 = TestHelper.SolutionFolder.Combine(
                                        $"CKSetup.Runner/bin/{TestHelper.BuildConfiguration}/net461" );

        static public NormalizedPath[] AllComponents = new[]
        {
            StupidModelNetStandard20,
            StupidModelNet461,
            StupidEngineNetCoreApp20,
            StupidEngineNetCoreApp21,
            StupidEngineNet461,
            RunnerNetCoreApp20,
            RunnerNetCoreApp21,
            RunnerNet461,
        };

        static public void RegisterAllComponents( string storePath, bool forcePublish = false )
        {
            TestHelper.CKSetup.PublishAndAddComponentFoldersToStore( AllComponents.Select( p => p.ToString() ), forcePublish, storePath )
                .Should().BeTrue();
        }

    }
}
