using CK.Testing;
using CK.Text;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static CK.Testing.CKSetupTestHelper;


namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class ComponentDBTests
    {
        [TestCase( "component-db-v0.zip" )]
        public void component_load_stores( string name )
        {
            var db = LoadFromCapturedStore( name );
            var info = new ComponentDBInfo( db );
        }

        static ComponentDB LoadFromCapturedStore( string name )
        {
            var path = TestHelper.SolutionFolder.Combine( "Tests/CKSetup.Core.Tests/CapturedStores" ).AppendPart( name );
            using( var a = ZipFile.OpenRead( path ) )
            using( var content = a.Entries[0].Open() )
            {
                return new ComponentDB( XDocument.Load( content ).Root );
            }
        }

        [Test]
        public void removing_components()
        {
            NormalizedPath storePath = TestHelper.CKSetup.GetTestStorePath( CKSetupStoreType.Directory );
            TestHelper.CKSetup.AddComponentFoldersToStore( new string[]
            {
                TestHelper.SolutionFolder.Combine( $"Tests/StupidEngine/bin/{TestHelper.BuildConfiguration}/net461" ),
                TestHelper.SolutionFolder.Combine( $"Tests/StupidModel/bin/{TestHelper.BuildConfiguration}/net461" )
            }, storePath ).Should().BeTrue();
            TestHelper.CKSetup.RemoveComponentsFromStore( c => true, true, storePath ).Should().Be( 2 );
            TestHelper.CKSetup.AddComponentFoldersToStore( new string[]
            {
                TestHelper.SolutionFolder.Combine( $"Tests/StupidEngine/bin/{TestHelper.BuildConfiguration}/net461" ),
                TestHelper.SolutionFolder.Combine( $"Tests/StupidModel/bin/{TestHelper.BuildConfiguration}/net461" )
            }, storePath ).Should().BeTrue();
        }
    }
}
