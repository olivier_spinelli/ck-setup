using CK.Core;
using CK.Testing;
using CK.Text;
using FluentAssertions;
using NUnit.Framework;
using System.Xml.Linq;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class RunningStupidEngineTests
    {
        [Test]
        public void checkConfig()
        {
            TestHelper.CKSetup.DefaultStorePath.Should().Be( TestHelper.TestProjectFolder.Combine( "../TempStore" ).ResolveDots() );
        }

        [TearDown]
        public void ResetCKSetupWorkingDirectory()
        {
            TestHelper.CKSetup.WorkingDirectory = null;
        }

        [TestCase( "6.0.0-a00-00-develop-0032", null )]
        [TestCase( "UseLocalRunner", "SetupWorkingDir" )]
        public void run_setup_on_StupidModel_net461( string runnerVersion, string setupWorkingDir )
        {
            if( setupWorkingDir != null )
            {
                TestHelper.CKSetup.WorkingDirectory = TestHelper.TestProjectFolder.Combine( setupWorkingDir );
            }
            bool localRunner = runnerVersion == "UseLocalRunner";
            NormalizedPath storePath = TestHelper.CKSetup.GetTestStorePath( CKSetupStoreType.Directory );

            TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { LocalHelper.StupidEngineNet461, LocalHelper.StupidModelNet461 }, storePath )
                .Should().BeTrue();

            var conf = new SetupConfiguration();
            if( localRunner )
            {
                TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { LocalHelper.RunnerNet461 }, storePath )
                    .Should().BeTrue();
            }
            else conf.Dependencies.Add( new SetupDependency( "CKSetup.Runner", CSemVer.SVersion.Parse( runnerVersion ) ) );


            NormalizedPath modelPath = LocalHelper.StupidModelNet461;
            conf.BinPaths.Add( modelPath );
            conf.Configuration = new XElement("StupidConf");
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null, launchDebug: false, forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );

            TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null, launchDebug: false, forceSetup: false )
                .Should().Be( CKSetupRunResult.UpToDate );

            conf.ExternalSignature = "something changed...";
            TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null, launchDebug: false, forceSetup: false )
                .Should().Be( CKSetupRunResult.Succeed );

        }

        [TestCase( "6.0.0-a00-00-develop-0032" )]
        [TestCase( "UseLocalRunner" )]
        public void run_setup_on_StupidModel_netcore( string runnerVersion )
        {
            bool localRunner = runnerVersion == "UseLocalRunner";
            NormalizedPath storePath = TestHelper.CKSetup.GetTestStorePath( CKSetupStoreType.Directory );

            string pubEnginePath20 = TestHelper.CKSetup.DotNetPublish( LocalHelper.StupidEngineNetCoreApp20 );
            TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { pubEnginePath20, LocalHelper.StupidModelNetStandard20 }, storePath )
                .Should().BeTrue();

            var conf = new SetupConfiguration();
            conf.PreferredTargetRuntimes.Add( TargetRuntime.NetCoreApp20 );
            if( localRunner )
            {
                string pubRunnerPath20 = TestHelper.CKSetup.DotNetPublish( LocalHelper.RunnerNetCoreApp20 );
                TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { pubRunnerPath20 }, storePath )
                    .Should().BeTrue();
            }
            else conf.Dependencies.Add( new SetupDependency( "CKSetup.Runner", CSemVer.SVersion.Parse( runnerVersion ) ) );

            NormalizedPath modelPath = LocalHelper.StupidModelNetStandard20;

            conf.BinPaths.Add( modelPath );
            conf.Configuration = new XElement( "StupidConf" );
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null, launchDebug: false, forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );

            TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null, launchDebug: false, forceSetup: false )
                .Should().Be( CKSetupRunResult.UpToDate );

            conf.ExternalSignature = "something";
            TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null, launchDebug: false, forceSetup: false )
                .Should().Be( CKSetupRunResult.Succeed );
        }

        [TestCase( "UseLocalRunner", TargetRuntime.NetCoreApp21 )]
        [TestCase( "UseLocalRunner", TargetRuntime.NetCoreApp20 )]
        public void run_setup_on_StupidModel_netcore20_or_21( string runnerVersion, TargetRuntime preferred )
        {
            bool localRunner = runnerVersion == "UseLocalRunner";
            NormalizedPath storePath = TestHelper.CKSetup.GetTestStorePath( CKSetupStoreType.Directory );

            string pubEnginePath20 = TestHelper.CKSetup.DotNetPublish( LocalHelper.StupidEngineNetCoreApp20 );
            string pubEnginePath21 = TestHelper.CKSetup.DotNetPublish( LocalHelper.StupidEngineNetCoreApp21 );
            TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { pubEnginePath20, pubEnginePath21, LocalHelper.StupidModelNetStandard20 }, storePath )
                .Should().BeTrue();

            var conf = new SetupConfiguration();
            if( localRunner )
            {
                string pubRunnerPath20 = TestHelper.CKSetup.DotNetPublish( LocalHelper.RunnerNetCoreApp20 );
                string pubRunnerPath21 = TestHelper.CKSetup.DotNetPublish( LocalHelper.RunnerNetCoreApp21 );
                TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { pubRunnerPath20, pubRunnerPath21 }, storePath )
                    .Should().BeTrue();
            }
            else conf.Dependencies.Add( new SetupDependency( "CKSetup.Runner", CSemVer.SVersion.Parse( runnerVersion ) ) );

            NormalizedPath modelPath = LocalHelper.StupidModelNetStandard20;

            conf.BinPaths.Add( modelPath );
            conf.Configuration = new XElement( "StupidConf" );
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";
            conf.PreferredTargetRuntimes.Add( preferred );

            TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null, launchDebug: false, forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );

            TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null, launchDebug: false, forceSetup: false )
                .Should().Be( CKSetupRunResult.UpToDate );

            conf.ExternalSignature = "something";
            TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null, launchDebug: false, forceSetup: false )
                .Should().Be( CKSetupRunResult.Succeed );
        }

    }
}
