using CK.Core;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using CK.Monitoring.InterProcess;
using System.Collections.Generic;
using System.Linq;

#if !NET461
using Microsoft.Extensions.DependencyModel;
#endif

namespace CKSetup.Runner
{
    static class ActualRunner
    {
        static public int Run( StringBuilder rawLogText, string[] args, Func<IReadOnlyList<AssemblyLoadConflict>> monitoredConflicts )
        {
            SimplePipeSenderActivityMonitorClient pipeClient;
            IActivityMonitor monitor = CreateMonitor( rawLogText, args, out pipeClient );
            using( monitor.OpenLog( LogLevel.Info, $"Starting {Program.HeaderVersion}" ) )
            {
                try
                {
                    if( Array.IndexOf( args, "merge-deps" ) >= 0 )
                    {
                        MergeDeps( monitor );
                        return 0;
                    }
                    XElement root = XDocument.Load( Path.Combine( AppContext.BaseDirectory, "CKSetup.Runner.Config.xml" ) ).Root;
                    XElement ckSetup = root.Element( "CKSetup" );
                    string engineAssemblyQualifiedName = ckSetup?.Element( "EngineAssemblyQualifiedName" )?.Value;
                    if( string.IsNullOrWhiteSpace( engineAssemblyQualifiedName ) )
                    {
                        monitor.Log( LogLevel.Fatal, "Missing element CKSetup/EngineAssemblyQualifiedName or empty value." );
                        return 3;
                    }
                    Type runnerType = SimpleTypeFinder.WeakResolver( engineAssemblyQualifiedName, true );
                    object runner = Activator.CreateInstance( runnerType, monitor, root );
                    MethodInfo m = runnerType.GetMethod( "Run" );
                    if( m.ReturnType == typeof(bool) )
                    {
                        return (bool)m.Invoke( runner, Array.Empty<object>() ) ? 0 : 3;
                    }
                    monitor.Log( LogLevel.Warn, $"Runner '{runnerType}': Run method should return a boolean." );
                    m.Invoke( runner, Array.Empty<object>() );
                    return 0;
                }
                catch( Exception ex )
                {
                    monitor.Log( LogLevel.Fatal, ex.Message, ex );
                    return 2;
                }
                finally
                {
                    var conflicts = monitoredConflicts();
                    if( conflicts.Count == 0 )
                    {
                        monitor.Log( LogLevel.Info, "No assembly load conflicts." );
                    }
                    else using( monitor.OpenLog( LogLevel.Warn, $"{conflicts.Count} assembly load conflicts:" ) )
                    {
                        foreach( var c in conflicts )
                        {
                            monitor.Log( LogLevel.Warn, c.ToString() );
                        }
                    }
                    pipeClient?.Dispose();
                }
            }
        }

        static IActivityMonitor CreateMonitor( StringBuilder rawLogText, string[] args, out SimplePipeSenderActivityMonitorClient pipeClient )
        {
            pipeClient = null;
            var monitor = new ActivityMonitor();
            bool hasLogPipe = false;
            foreach( var a in args )
            {
                if( a.StartsWith( "/logPipe:" ) && a.Length > 9 )
                {
                    pipeClient = new SimplePipeSenderActivityMonitorClient( a.Substring( 9 ) );
                    monitor.Output.RegisterClient( pipeClient );
                    hasLogPipe = true;
                }
            }
            if( !hasLogPipe )
            {
                if( Program.SilentMode )
                {
                    rawLogText.AppendLine( "Missing /logPipe: parameter. Running in /silent mode." );
                }
                else
                {
                    monitor.Output.RegisterClient( new ActivityMonitorConsoleClient() );
                    monitor.Log( LogLevel.Warn, "Missing /logPipe: parameter. Using Console." );
                }
            }
            return monitor;
        }

        static void MergeDeps( IActivityMonitor m )
        {
#if NET461
            throw new ArgumentException( "Invalid merge-deps argument in .Net Framework." );
#else
            // This seems completely stupid... but ".NetCoreApp..." < ".NetStandard..." (and that's
            // what we are looking for right now) then we try to privilegiate "small" dependencies
            // (so that the order has a chance to follow the actual dependency order) and ultimately
            // we ensure deterministic order (based on the deps.json file name).
            // This may require more work (".NETCoreApp,Version=v2.0" vs. ".NETCoreApp,Version=v2.1"?)
            // in the future.
            int CompareTargetFramework( (string Name, DependencyContext Dep) c1, (string Name, DependencyContext Dep) c2 )
            {
                int cmp = c1.Dep.Target.Framework.CompareTo( c2.Dep.Target.Framework );
                if( cmp != 0 ) return cmp;
                // Privilegiates deps.json with a small number of RuntimeLibraries.
                cmp = c1.Dep.RuntimeLibraries.Count - c2.Dep.RuntimeLibraries.Count;
                if( cmp != 0 ) return cmp;
                // Privilegiates deps.json with little RuntimeGraph.
                cmp = c1.Dep.RuntimeGraph.Count - c2.Dep.RuntimeGraph.Count;
                if( cmp != 0 ) return cmp;
                return c1.Name.CompareTo( c2.Name );
            }

            using( m.OpenLog( LogLevel.Info, "Merging deps.json files." ) )
            {
                DependencyContext c = DependencyContext.Default;
                using( var depsReader = new DependencyContextJsonReader() )
                {
                    var others = new List<(string,DependencyContext)>();
                    foreach( var file in Directory.EnumerateFiles( AppContext.BaseDirectory, "*.deps.json" ) )
                    {
                        var name = Path.GetFileName( file );
                        if( name == "CKSetup.Runner.deps.json" ) continue;

                        try
                        {
                            using( var content = File.OpenRead( file ) )
                            {
                                var o = depsReader.Read( content );
                                others.Add( (name, o) );
                                m.Log( LogLevel.Info, $"'{name}' - {o.Target.Framework}." );
                            }
                        }
                        catch( Exception ex )
                        {
                            m.Log( LogLevel.Fatal, $"While reading '{name}'.", ex );
                        }
                    }
                    others.Sort( CompareTargetFramework );
                    foreach( var d in others )
                    {
                        m.Log( LogLevel.Info, $"Merging '{d.Item1}' - {d.Item2.Target.Framework}." );
                        c = c.Merge( d.Item2 );
                    }
                }
                // Check for Compile/Runtime conflicts
                IEnumerable<string> compileNames = c.CompileLibraries.Select( x => x.Name );
                IEnumerable<string> runtimeNames = c.RuntimeLibraries.Select( x => x.Name );
                IEnumerable<string> commonLibraryNames = compileNames.Intersect( runtimeNames );
                if( commonLibraryNames.Any() )
                {
                    List<string> compileLibsToRemove = new List<string>();
                    foreach( var libraryName in commonLibraryNames )
                    {
                        var compileLib = c.CompileLibraries.First( x => x.Name == libraryName );
                        var runtimeLib = c.RuntimeLibraries.First( x => x.Name == libraryName );
                        if( compileLib.Version != runtimeLib.Version )
                        {
                            using( m.OpenLog( LogLevel.Warn, $"Library '{libraryName}' was merged in both Compile and Runtime libraries with different versions:" ) )
                            {
                                m.Log( LogLevel.Warn, $"(removed) Compile: {compileLib.Name} {compileLib.Version} at {compileLib.Path}" );
                                m.Log( LogLevel.Warn, $"   (kept) Runtime: {runtimeLib.Name} {runtimeLib.Version} at {runtimeLib.Path}" );
                            }
                            compileLibsToRemove.Add( libraryName );
                        }
                    }

                    if( compileLibsToRemove.Count > 0 )
                    {
                        // Rewrite DependencyContext, excluding problematic libraries
                        c = new DependencyContext(
                            c.Target,
                            c.CompilationOptions,
                            c.CompileLibraries.Where( l => !compileLibsToRemove.Contains( l.Name ) ),
                            c.RuntimeLibraries,
                            c.RuntimeGraph );
                    }
                }

                m.Log( LogLevel.Info, "Saving 'CKSetup.Runner.deps.json.merged'." );
                var path = Path.Combine( AppContext.BaseDirectory, "CKSetup.Runner.deps.json.merged" );
                var writer = new DependencyContextWriter();
                using( var output = File.Open( path, FileMode.Create, FileAccess.Write, FileShare.None ) )
                {
                    writer.Write( c, output );
                }
            }
#endif
        }
    }
}
