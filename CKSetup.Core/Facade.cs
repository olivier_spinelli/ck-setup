using CK.Core;
using CK.Monitoring.InterProcess;
using CK.Text;
using CSemVer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// This facade encapsulates the CKSetup run operations.
    /// </summary>
    static public class Facade
    {
        /// <summary>
        /// The default store is the directory Environment.SpecialFolder.LocalApplicationData/CKSetupStore.
        /// This is the default shared store that is user and machine specific.
        /// </summary>
        public static readonly string DefaultStorePath = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ), "CKSetupStore" );

        /// <summary>
        /// The default remote store is https://cksetup.invenietis.net.
        /// </summary>
        public static readonly Uri DefaultStoreUrl = new Uri( "https://cksetup.invenietis.net" );

        /// <summary>
        /// Locates a store that must be used.
        /// This method never fails and ultimately defaults to <see cref="DefaultStorePath"/>.
        /// If a <paramref name="storePath"/> is provided, it will be used (it it does not already exist, a warning is emitted).
        /// </summary>
        /// <param name="m">The monitor.</param>
        /// <param name="storePath">A path to an existing store or to a store that will be created.</param>
        /// <param name="startPath">An optional path to look for a store up to the root.</param>
        /// <returns>True on success, false on error.</returns>
        public static string LocateStorePath( IActivityMonitor m, string storePath = null, string startPath = null )
        {
            var result = storePath;
            if( string.IsNullOrEmpty( result ) )
            {
                result = FindStorePathFrom( startPath );
                if( result == null )
                {
                    result = FindStorePathFrom( AppContext.BaseDirectory );
                }
                if( result == null )
                {
                    result = DefaultStorePath;
                }
            }
            else
            {
                result = Path.GetFullPath( result );
                if( !File.Exists( result ) && !Directory.Exists( result ) )
                {
                    m.Warn( $"The provided store '{result}' does not exist. It will be created." );
                }
            }
            return result;
        }

        static string FindStorePathFrom( string dir )
        {
            while( !string.IsNullOrEmpty( dir ) )
            {
                string test = Path.Combine( dir, "CKSetupStore.zip" );
                if( File.Exists( test ) ) return test;
                test = Path.Combine( dir, "CKSetupStore" );
                if( Directory.Exists( test ) ) return test;
                dir = Path.GetDirectoryName( dir );
            }
            return null;
        }

        /// <summary>
        /// Read multiple <see cref="BinFolder"/>.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="paths">Folder paths to read.</param>
        /// <returns>The list of bin folders or null if an error occurred.</returns>
        public static List<BinFolder> ReadBinFolders( IActivityMonitor monitor, IEnumerable<string> paths )
        {
            var result = new List<BinFolder>();
            using( monitor.OpenDebug( "Discovering files." ) )
            {
                try
                {
                    foreach( var d in paths )
                    {
                        var f = BinFolder.ReadBinFolder( monitor, d );
                        if( f == null ) return null;
                        result.Add( f );
                    }
                }
                catch( Exception ex )
                {
                    monitor.Fatal( ex );
                    return null;
                }
            }
            if( result.Count == 0 )
            {
                monitor.Error( "No valid runtime files found." );
                return null;
            }
            return result;
        }

        /// <summary>
        /// Main entry point to CKSetup run.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="store">The opened, valid, local store.</param>
        /// <param name="config">The setup configuration.</param>
        /// <param name="missingImporter">Optional component importer. Can be null.</param>
        /// <param name="launchDebug">True to call <see cref="Debugger.Launch"/> at the very start of the runner process.</param>
        /// <param name="forceSetup">True to setup even if <see cref="SetupConfiguration.UseSignatureFiles"/> is true and the signature files match.</param>
        /// <returns>The <see cref="CKSetupRunResult"/> enum.</returns>
        public static CKSetupRunResult DoRun(
            IActivityMonitor monitor,
            LocalStore store,
            SetupConfiguration config,
            IComponentImporter missingImporter,
            bool launchDebug,
            bool forceSetup )
        {
            if( config == null ) throw new ArgumentNullException( nameof( config ) );
            using( monitor.OpenInfo( "Running Setup." ) )
            {
                var r = ActualRun();
                monitor.CloseGroup( r );
                return r;
            }

            CKSetupRunResult ActualRun()
            { 
                SetupContext ctx = SetupContext.Create( monitor, config, forceSetup );
                if( !ctx.Succes ) return CKSetupRunResult.Failed;
                if( ctx.UpToDate ) return CKSetupRunResult.UpToDate;
                try
                {
                    Debug.Assert( ctx.FinalWorkingDirectory[ctx.FinalWorkingDirectory.Length - 1] == Path.DirectorySeparatorChar );
                    Debug.Assert( Directory.Exists( ctx.FinalWorkingDirectory ) );
                    IEnumerable<SetupDependency> manualDependencies = config.Dependencies;

                    // If no explicit dependencies have been set on CKSetup.Runner...
                    if( !manualDependencies.Any( d => d.UseName == "CKSetup.Runner" ) )
                    {
                        var thisAssembly = (AssemblyInformationalVersionAttribute)Attribute.GetCustomAttribute( Assembly.GetExecutingAssembly(), typeof( AssemblyInformationalVersionAttribute ) );
                        var thisVersion = new InformationalVersion( thisAssembly?.InformationalVersion );
                        // ...use the ZeroVersion when compiling uncomitted work
                        // ...or this CKSetup.Core version.
                        SVersion runnerVersion = thisVersion.SemVersion == null
                                                    ? SVersion.ZeroVersion
                                                    : thisVersion.NuGetVersion;
                        manualDependencies = manualDependencies.Append( new SetupDependency( "CKSetup.Runner", runnerVersion ) );
                    }
                    var extractResult = store.ExtractRuntimeDependencies( ctx.FinalWorkingDirectory, ctx.BinFolders, missingImporter, manualDependencies, config.PreferredTargetRuntimes );
                    if( !extractResult.Success )
                    {
                        return CKSetupRunResult.Failed;
                    }
                    int count = ctx.DedupFiles.Count;
                    using( monitor.OpenInfo( $"Copying {count} files from bin folders." ) )
                    {
                        var dedupFolders = new HashSet<string>( ctx.DedupFiles.Keys.Select( p => Path.GetDirectoryName( p ) ), StringComparer.OrdinalIgnoreCase );
                        foreach( var requiredFolder in dedupFolders )
                        {
                            string targetPath = ctx.FinalWorkingDirectory + requiredFolder;
                            if( !Directory.Exists( targetPath ) )
                            {
                                monitor.Debug( $"Creating '{targetPath}' directory." );
                                Directory.CreateDirectory( targetPath );
                            }
                        }
                        foreach( var f in ctx.DedupFiles.Values )
                        {
                            string targetPath = ctx.FinalWorkingDirectory + f.LocalFileName;
                            if( !File.Exists( targetPath ) )
                            {
                                File.Copy( f.FullPath, targetPath );
                                --count;
                            }
                        }
                        monitor.CloseGroup( $"Skipped {count} file(s)." );
                    }
                    using( monitor.OpenInfo( $"Generating CKSetup.Runner.Config.xml file." ) )
                    {
                        var root = new XElement( config.Configuration );
                        var ckSetup = new XElement( SetupConfiguration.xCKSetup );
                        ckSetup.Add( new XElement( SetupConfiguration.xBasePath, ctx.BasePath ) );
                        ckSetup.Add( new XElement( SetupConfiguration.xEngineAssemblyQualifiedName, config.EngineAssemblyQualifiedName ) );
                        ckSetup.Add( new XElement( SetupConfiguration.xRunSignature, ctx.CurrentSignature.ToString() ) );
                        var binPaths = new XElement( SetupConfiguration.xBinPaths );
                        foreach( BinFolder f in ctx.BinFolders )
                        {
                            var binPath = new XElement( SetupConfiguration.xBinPath, new XAttribute( SetupConfiguration.xBinPath, f.BinPath ) );
                            var models = f.Assemblies.Where( a => a.ComponentKind == ComponentKind.Model );
                            var dependencies = f.Assemblies.Where( a => a.ComponentKind == ComponentKind.SetupDependency );
                            var modelDependents = f.Assemblies.Where( a => a.ComponentKind == ComponentKind.None && a.LocalDependencies.Any( dep => dep.ComponentKind == ComponentKind.Model ) );
                            binPath.Add( models.Select( a => new XElement( SetupConfiguration.xModel, a.Name.Name ) ) );
                            binPath.Add( dependencies.Select( a => new XElement( SetupConfiguration.xDependency, a.Name.Name ) ) );
                            binPath.Add( modelDependents.Select( a => new XElement( SetupConfiguration.xModelDependent, a.Name.Name ) ) );
                            binPaths.Add( binPath );
                        }
                        ckSetup.Add( binPaths );
                        root.Add( ckSetup );
                        monitor.Debug( root.ToString() );
                        new XDocument( root ).Save( Path.Combine( ctx.FinalWorkingDirectory, "CKSetup.Runner.Config.xml" ) );
                    }
                    if( RunSetupRunner( monitor, ctx.FinalWorkingDirectory, launchDebug ) )
                    {
                        UpdateFileSignatures( monitor, ctx.BinFolders, ctx.CurrentSignature );
                        return CKSetupRunResult.Succeed;
                    }
                    UpdateFileSignatures( monitor, ctx.BinFolders, SHA1Value.ZeroSHA1 );
                }
                catch( Exception ex )
                {
                    monitor.Fatal( ex );
                }
                finally
                {
                    if( ctx.FinalWorkingDirectory.StartsWith( Path.GetTempPath() ) )
                    {
                        DeleteTemporaryWorkingDirectory( monitor, ctx.FinalWorkingDirectory );
                    }
                    else monitor.Trace( $"Leaving Working Directory '{ctx.FinalWorkingDirectory}' as-is." );
                }
                return CKSetupRunResult.Failed;
            }
        }

        static void UpdateFileSignatures( IActivityMonitor monitor, IReadOnlyList<BinFolder> binFolders, SHA1Value currentSignature )
        {
            bool delete = currentSignature == SHA1Value.ZeroSHA1;
            using( monitor.OpenTrace( delete ? "Deleting signature files." : "Updating signature files." ) )
            {
                foreach( var f in binFolders )
                {
                    try
                    {
                        var fSign = Path.Combine( f.BinPath, SetupContext.SignatureFileName );
                        if( currentSignature == SHA1Value.ZeroSHA1 )
                        {
                            if( File.Exists( fSign ) ) File.Delete( fSign );
                        }
                        else
                        {
                            File.WriteAllText( fSign, currentSignature.ToString() );
                        }
                    }
                    catch( Exception ex )
                    {
                        monitor.Warn( ex );
                    }
                }
            }
        }

        static void DeleteTemporaryWorkingDirectory( IActivityMonitor monitor, string workingDir )
        {
            using( monitor.OpenInfo( $"Deleting temporary Working Directory." ) )
            {
                int retryCount = 0;
                for(; ; )
                {
                    try
                    {
                        Directory.Delete( workingDir, true );
                        break;
                    }
                    catch( Exception ex )
                    {
                        if( ++retryCount < 3 )
                        {
                            monitor.Warn( $"Error while deleting folder. Retrying (count = {retryCount}).", ex );
                            System.Threading.Thread.Sleep( 500 );
                        }
                        else
                        {
                            monitor.Error( $"Unable to delete folder '{workingDir}'.", ex );
                            break;
                        }
                    }
                }
            }
        }

        static bool RunSetupRunner(
            IActivityMonitor m,
            string workingDir,
            bool launchDebug )
        {
            using( m.OpenInfo( "Launching CKSetup.Runner process." ) )
            {
                string exe = Path.Combine( workingDir, "CKSetup.Runner.exe" );
                string dll = Path.Combine( workingDir, "CKSetup.Runner.dll" );
                string fileName, arguments;
                if( !File.Exists( exe ) )
                {
                    if( !File.Exists( dll ) )
                    {
                        m.Error( "Unable to find CKSetup.Runner in folder." );
                        return false;
                    }
                    fileName = "dotnet";

#region Using existing runtimeconfig.json to create CKSetup.Runner.runtimeconfig.json: (only for old runner version <= 6.0.0-rc.5)
                    // We now capture the .runtimeconfig.json in the component files. Playing with runtimeconfig as we did before 6.0.0-rc.6 is useless.
                    var runnerVersion = InformationalVersion.Parse( FileVersionInfo.GetVersionInfo( dll ).ProductVersion ).SemVersion;
                    if( runnerVersion != SVersion.ZeroVersion && runnerVersion <= SVersion.Create( 6, 0, 0, "rc.5" ) )
                    {
                        m.Info( $"Legacy trick: runner version is {runnerVersion}: runtimeconfig.json was not captured and was infered from existing ones..." );
                        bool FindRuntimeConfigFiles( IActivityMonitor monitor, string binPath, out string runtimeDevPath, out string runtimePath )
                        {
                            runtimeDevPath = runtimePath = null;
                            // First find the dev file.
                            var devs = Directory.EnumerateFiles( binPath, "*.runtimeconfig.dev.json" ).ToList();
                            if( devs.Count > 1 )
                            {
                                monitor.Error( $"Found more than one runtimeconfig.dev.json files: '{String.Join( "', '", devs.Select( p => Path.GetFileName( p ) ) ) }'." );
                                return false;
                            }
                            if( devs.Count == 1 )
                            {
                                runtimeDevPath = devs[0];
                                string fRtDevName = Path.GetFileName( runtimeDevPath );
                                monitor.Trace( $"Found '{fRtDevName}'." );
                                string fRtName = fRtDevName.Remove( fRtDevName.Length - 9, 4 );
                                runtimePath = Path.Combine( binPath, fRtName );
                                if( !File.Exists( runtimePath ) )
                                {
                                    monitor.Error( $"Unable to find '{fRtName}' file (but found '{fRtDevName}')." );
                                    return false;
                                }
                            }
                            else
                            {
                                var rtFiles = Directory.EnumerateFiles( binPath, "*.runtimeconfig.json" ).ToList();
                                if( rtFiles.Count > 1 )
                                {
                                    monitor.Error( $"Found more than one runtimeconfig.json files: '{String.Join( "', '", rtFiles.Select( p => Path.GetFileName( p ) ) ) }'." );
                                    return false;
                                }
                                else if( rtFiles.Count == 0 )
                                {
                                    // When there is NO runtimeconfig.json file, this is not an error:
                                    // We'll use a default one (and pray).
                                    monitor.Warn( $"Unable to find a runtimeconfig.json file." );
                                    return true;
                                }
                                runtimePath = rtFiles[0];
                            }
                            return true;
                        }

                        const string runnerConfigFileName = "CKSetup.Runner.runtimeconfig.json";
                        // We try to find an existing runtimeconfig.json:
                        // - First look for unique runtimeconfig.dev.json
                        //    - It must be unique and have an associated runtimeconfig.json file otherwise we fail.
                        //    - If no dev file exists, we look for a unique runtimeconfig.json
                        // - If there is runtimeconfig.json duplicates in the folder, we fail.
                        // - If there is no runtimeconfig.json, we generate a default one.
                        if( !FindRuntimeConfigFiles( m, workingDir, out string fRtDevPath, out string fRtPath ) )
                        {
                            return false;
                        }
                        string runnerFile = Path.Combine( workingDir, runnerConfigFileName );
                        // If there is a dev file, extracts its additional probe paths.
                        string additionalProbePaths = ExtractJsonAdditonalProbePaths( m, fRtDevPath );
                        if( additionalProbePaths == null )
                        {
                            if( fRtPath == null )
                            {
                                const string defaultRt = "{\"runtimeOptions\":{\"tfm\":\"netcoreapp2.0\",\"framework\":{\"name\":\"Microsoft.NETCore.App\",\"version\": \"2.0.0\"}}}";
                                m.Info( $"Trying with a default {runnerConfigFileName}: {defaultRt}" );
                                File.WriteAllText( runnerFile, defaultRt );
                            }
                            else File.Copy( fRtPath, runnerFile, true );
                        }
                        else
                        {
                            string txt = File.ReadAllText( fRtPath );
                            int idx = txt.LastIndexOf( '}' ) - 1;
                            if( idx > 0 ) idx = txt.LastIndexOf( '}', idx ) - 1;
                            if( idx > 0 )
                            {
                                txt = txt.Insert( idx, "," + additionalProbePaths );
                                File.WriteAllText( runnerFile, txt );
                            }
                            else
                            {
                                using( m.OpenError( "Unable to inject additionalProbingPaths in:" ) )
                                {
                                    m.Error( txt );
                                }
                                return false;
                            }
                        }
                    }
#endregion

#region Merging all deps.json into CKSetup.Runner.deps.json
                    {
                        arguments = "CKSetup.Runner.dll merge-deps";
                        if( launchDebug ) arguments += " /launchDebug";
                        if( !RunCKSetupRunnerProcess( m, workingDir, fileName, arguments ) )
                        {
                            return false;
                        }
                        string theFile = Path.Combine( workingDir, "CKSetup.Runner.deps.json" );
                        string theBackup = theFile + ".original";
                        File.Replace( theFile + ".merged", theFile, theBackup );
                    }
#endregion
                    arguments = "CKSetup.Runner.dll ";
                }
                else
                {
                    fileName = exe;
                    arguments = String.Empty;
                }
                if( launchDebug ) arguments += "/launchDebug ";
                return RunCKSetupRunnerProcess( m, workingDir, fileName, arguments );
            }
        }

        static bool RunCKSetupRunnerProcess(
                IActivityMonitor m,
                string workingDir,
                string fileName,
                string arguments )
        {
            ProcessStartInfo cmdStartInfo = new ProcessStartInfo
            {
                WorkingDirectory = workingDir,
                UseShellExecute = false,
                CreateNoWindow = true,
                FileName = fileName,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
            };
            using( var logReceiver = SimpleLogPipeReceiver.Start( m, true ) )
            {
                cmdStartInfo.Arguments = arguments;
                cmdStartInfo.Arguments += " /silent /logPipe:" + logReceiver.PipeName;
                using( m.OpenTrace( $"{fileName} {cmdStartInfo.Arguments}" ) )
                using( Process cmdProcess = new Process() )
                {
                    StringBuilder conOut = new StringBuilder();
                    cmdProcess.StartInfo = cmdStartInfo;
                    cmdProcess.ErrorDataReceived += ( o, e ) => { if( !string.IsNullOrEmpty( e.Data ) ) conOut.Append( "<StdErr> " ).AppendLine( e.Data ); };
                    cmdProcess.OutputDataReceived += ( o, e ) => { if( e.Data != null ) conOut.Append( "<StdOut> " ).AppendLine( e.Data ); };
                    cmdProcess.Start();
                    cmdProcess.BeginErrorReadLine();
                    cmdProcess.BeginOutputReadLine();
                    cmdProcess.WaitForExit();

                    if( conOut.Length > 0 ) m.Info( conOut.ToString() );
                    var endLogStatus = logReceiver.WaitEnd( cmdProcess.ExitCode != 0 );
                    if( endLogStatus != LogReceiverEndStatus.Normal )
                    {
                        m.Warn( $"Pipe log channel abnormal end status: {endLogStatus}." );
                    }
                    if( cmdProcess.ExitCode != 0 )
                    {
                        const string rawLogFileName = "CKSetup.Runner.RawLogs.txt";
                        using( m.OpenError( $"Process returned ExitCode {cmdProcess.ExitCode}. {rawLogFileName} file content:" ) )
                        {
                            var rawLog = Path.Combine( workingDir, rawLogFileName );
                            if( File.Exists( rawLog ) )
                            {
                                m.Debug( File.ReadAllText( rawLog ) );
                            }
                            else m.CloseGroup( $"No {rawLogFileName} file." );
                        }
                        return false;
                    }
                    return true;
                }
            }
        }

        static string ExtractJsonAdditonalProbePaths( IActivityMonitor m, string fRtDevPath )
        {
            if( fRtDevPath == null ) return null;
            string txt = File.ReadAllText( fRtDevPath );
            var matcher = new StringMatcher( txt );
            List<KeyValuePair<string, object>> oRoot;
            int idxRuntimeOptions;
            List<KeyValuePair<string, object>> oRuntimeOptions;
            int idxAdditionalProbingPaths;
            List<object> additionalProbingPathsObj;
            if( matcher.MatchJSONObject( out object oConfig )
                && (oRoot = (oConfig as List<KeyValuePair<string, object>>)) != null
                && (idxRuntimeOptions = oRoot.IndexOf( kv => kv.Key == "runtimeOptions" )) >= 0
                && (oRuntimeOptions = oRoot[idxRuntimeOptions].Value as List<KeyValuePair<string, object>>) != null
                && (idxAdditionalProbingPaths = oRuntimeOptions.IndexOf( kv => kv.Key == "additionalProbingPaths" )) >= 0
                && (additionalProbingPathsObj = oRuntimeOptions[idxAdditionalProbingPaths].Value as List<object>) != null
                && additionalProbingPathsObj.All( p => p is string ) )
            {
                m.Trace( $"Extracted {additionalProbingPathsObj.OfType<string>().Concatenate()} from {fRtDevPath}." );
                return "\"additionalProbingPaths\":[\""
                            + additionalProbingPathsObj
                                .Select( p => ((string)p).Replace("\\","\\\\" ) )
                                .Concatenate( "\", \"" )
                            + "\"]";
            }
            using( m.OpenWarn( "Unable to extract any additional probing paths from:" ) )
            {
                m.Warn( txt );
            }
            return null;
        }

    }
}
