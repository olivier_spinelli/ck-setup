using CK.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// Captures informations related to the setup initialization.
    /// This class is currently internal but may be published if needed.
    /// </summary>
    class SetupContext
    {
        /// <summary>
        /// The signature file name that should contain the <see cref="CurrentSignature"/>.
        /// </summary>
        public static readonly string SignatureFileName = "CKSetup.signature.txt";

        SetupContext(
            bool success,
            SetupConfiguration conf,
            IReadOnlyList<BinFolder> folders,
            IReadOnlyDictionary<string,BinFileInfo> files,
            SHA1Value currentSignature,
            string workingDir,
            string basePath )
        {
            Succes = success;
            Configuration = conf;
            BinFolders = folders;
            DedupFiles = files;
            CurrentSignature = currentSignature;
            FinalWorkingDirectory = workingDir;
            BasePath = basePath;
        }

        /// <summary>
        /// Gets the configuration.
        /// This is a mutable object but it should no be modified once a <see cref="SetupContext"/> is built.
        /// </summary>
        public SetupConfiguration Configuration { get; }

        /// <summary>
        /// Gets the configuration base path.
        /// When <see cref="SetupConfiguration.BasePath"/> is empty, the current directory is used.
        /// </summary>
        public string BasePath { get; }

        /// <summary>
        /// Gets whether this context has been successfully created.
        /// </summary>
        public bool Succes { get; }

        /// <summary>
        /// Gets whether signatures files have been checked and system is up to date:
        /// the <see cref="FinalWorkingDirectory"/> is null since no setup should be done.
        /// </summary>
        public bool UpToDate => Succes && FinalWorkingDirectory == null;

        /// <summary>
        /// Gets the current signature based on the <see cref="Configuration"/> and the content of
        /// the <see cref="SetupConfiguration.BinPaths"/>.
        /// It is <see cref="SHA1Value.ZeroSHA1"/> if <see cref="Succes"/> is false.
        /// </summary>
        public SHA1Value CurrentSignature { get; }

        /// <summary>
        /// Gets the <see cref="SetupConfiguration.BinPaths"/> as <see cref="BinFolder"/>.
        /// Null if this context is invalid.
        /// </summary>
        public IReadOnlyList<BinFolder> BinFolders { get; }

        /// <summary>
        /// Gets all the files indexed by their local file names in the different bin paths.
        /// Null if this context is invalid.
        /// </summary>
        public IReadOnlyDictionary<string, BinFileInfo> DedupFiles { get; }

        /// <summary>
        /// Gets the working directory that must be used.
        /// It is either a sub directory of <see cref="Path.GetTempPath()"/> or the
        /// explicit <see cref="SetupConfiguration.WorkingDirectory"/>.
        /// This is null if <see cref="Succes"/> is false or <see cref="UpToDate"/> is true.
        /// </summary>
        public string FinalWorkingDirectory { get; }

        /// <summary>
        /// Creates a <see cref="SetupContext"/> from a <see cref="SetupConfiguration"/>.
        /// </summary>
        /// <param name="monitor">The monitor to use. Must not be null.</param>
        /// <param name="config">The configuration. Must not be null.</param>
        /// <param name="forceSetup">True to ignore <see cref="SetupConfiguration.UseSignatureFiles"/> configuration.</param>
        /// <returns>A setup context (potentially invalid, see <see cref="Succes"/>).</returns>
        public static SetupContext Create( IActivityMonitor monitor, SetupConfiguration config, bool forceSetup )
        {
            if( monitor == null ) throw new ArgumentNullException( nameof( monitor ) );
            if( config == null ) throw new ArgumentNullException( nameof( config ) );

            bool checkSignature = !forceSetup && config.UseSignatureFiles;
            using( monitor.OpenTrace( $"Initializing SetupContext{(checkSignature ? String.Empty : " (ignoring signatures)")}." ) )
            {
                monitor.Info( config.ToXml().ToString() );
                try
                {
                    string basePath = config.BasePath;
                    if( String.IsNullOrWhiteSpace( basePath ) )
                    {
                        basePath = Environment.CurrentDirectory;
                        monitor.Info( $"Configuration BasePath is empty: using current directory '{basePath}'." );
                    }

                    IReadOnlyDictionary<string, BinFileInfo> dedupFiles;
                    IReadOnlyList<BinFolder> folders = ReadBinFolders( monitor, basePath, config.BinPaths, out dedupFiles );
                    if( folders != null )
                    {
                        SHA1Value currentSignature = ComputeCurrentSignature( config, dedupFiles );
                        if( checkSignature
                            && currentSignature == ReadFileSignatures( monitor, folders ) )
                        {
                            monitor.CloseGroup( "Signature match. System is up to date." );
                            return new SetupContext( true, config, folders, dedupFiles, currentSignature, null, basePath );
                        }
                        string workingDir = GetWorkingDirectory( monitor, config.WorkingDirectory, folders );
                        if( workingDir != null )
                        {
                            if( checkSignature ) monitor.CloseGroup( "Signature does not match. Setup is required." );
                            return new SetupContext( true, config, folders, dedupFiles, currentSignature, workingDir, basePath );
                        }
                    }
                }
                catch( Exception ex )
                {
                    monitor.Fatal( ex );
                }
                monitor.CloseGroup( "Invalid SetupContext." );
                return new SetupContext( false, config, null, null, SHA1Value.ZeroSHA1, null, null );
            }
        }

        static SHA1Value ReadFileSignatures( IActivityMonitor monitor, IReadOnlyList<BinFolder> folders )
        {
            string current = null;
            foreach( var f in folders )
            {
                string fSign = Path.Combine( f.BinPath, SignatureFileName );
                if( !File.Exists( fSign ) )
                {
                    monitor.Info( $"Missing signature file '{fSign}'." );
                    return SHA1Value.ZeroSHA1;
                }
                var content = File.ReadAllText( fSign );
                if( current == null ) current = content;
                else if( current != content )
                {
                    monitor.Info( $"Signature mismatch between folders: {fSign} is not the same as the one in {folders[0].BinPath}." );
                    return SHA1Value.ZeroSHA1;
                }
            }
            if( SHA1Value.TryParse( current, 0, out var result ) )
            {
                monitor.Info( $"Found current signature among {folders.Count} folder(s): {result}." );
            }
            else
            {
                monitor.Warn( $"Unable to parse SHA1 signature: {current}." );
            }
            return result;
        }

        static SHA1Value ComputeCurrentSignature( SetupConfiguration config, IReadOnlyDictionary<string, BinFileInfo> dedupFiles )
        {
            using( SHA1Stream c = new SHA1Stream() )
            using( BinaryWriter cW = new BinaryWriter( c ) )
            {
                cW.Write( config.ToXml( forSignature: true ).ToString( SaveOptions.DisableFormatting ) );
                // Sort them to be hash code (non)ordering independent.
                var content = dedupFiles.Values.Select( f => f.ContentSHA1 ).ToArray();
                Array.Sort( content );
                foreach( var s in content ) s.Write( cW );
                return c.GetFinalResult();
            }
        }

        static IReadOnlyList<BinFolder> ReadBinFolders(
                    IActivityMonitor monitor,
                    string basePath,
                    IEnumerable<string> binPaths,
                    out IReadOnlyDictionary<string, BinFileInfo> dedupFiles )
        {
            Debug.Assert( !String.IsNullOrWhiteSpace( basePath ) && Path.IsPathRooted( basePath ) );
            dedupFiles = null;
            using( monitor.OpenInfo( "Reading binary folders." ) )
            {
                var folders = binPaths
                                .Select( p => Path.IsPathRooted( p ) ? p : Path.Combine( basePath, p ) )
                                .Select( p => BinFolder.ReadBinFolder( monitor, p ) ).ToList();
                if( folders.Any( f => f == null ) ) return null;
                if( folders.Count == 0 )
                {
                    monitor.Error( $"No BinPath provided. At least one is required." );
                    return null;
                }
                using( folders.Count > 1 ? monitor.OpenInfo( "Multiple Bin paths: Checking that common files are the same." ) : null )
                {
                    bool conflict = false;
                    Dictionary<string, BinFileInfo> byName = new Dictionary<string, BinFileInfo>();
                    foreach( var folder in folders )
                    {
                        foreach( var file in folder.Files )
                        {
                            if( byName.TryGetValue( file.LocalFileName, out var exists ) )
                            {
                                if( exists.ContentSHA1 != file.ContentSHA1 )
                                {
                                    conflict = true;
                                    using( monitor.OpenError( $"File mismatch: {file.LocalFileName}" ) )
                                    {
                                        monitor.Info( $"{exists.BinFolder.BinPath} has {exists.ToString()} with SHA1: {exists.ContentSHA1}." );
                                        monitor.Info( $"{file.BinFolder.BinPath} has {file.ToString()} with SHA1: {file.ContentSHA1}." );
                                    }
                                }
                            }
                            else byName.Add( file.LocalFileName, file );
                        }
                    }
                    if( conflict ) return null;
                    dedupFiles = byName;
                }
                monitor.CloseGroup( $"{folders.Count} folders read." );
                return folders;
            }
        }

        static string GetWorkingDirectory( IActivityMonitor monitor, string workingDir, IReadOnlyList<BinFolder> folders )
        {
            if( !String.IsNullOrWhiteSpace( workingDir ) )
            {
                workingDir = FileUtil.NormalizePathSeparator( Path.GetFullPath( workingDir ), true );
                if( folders.Any( b => b.BinPath.StartsWith( workingDir, StringComparison.OrdinalIgnoreCase ) ) )
                {
                    monitor.Error( $"Working directory can not be inside one of the bin paths." );
                    return null;
                }
                if( !Directory.Exists( workingDir ) )
                {
                    monitor.Info( $"Creating provided Working Directory: {workingDir}." );
                }
                workingDir = FileUtil.CreateUniqueTimedFolder( workingDir, null, DateTime.UtcNow );
                workingDir = FileUtil.NormalizePathSeparator( workingDir, true );
                monitor.Info( $"Created setup folder in Working Directory: {workingDir}." );
            }
            else
            {
                workingDir = FileUtil.CreateUniqueTimedFolder( Path.GetTempPath() + "CKSetup", null, DateTime.UtcNow );
                workingDir = FileUtil.NormalizePathSeparator( workingDir, true );
                monitor.Info( $"Created temporary Working Directory: {workingDir}." );
            }
            return workingDir;
        }

    }
}
