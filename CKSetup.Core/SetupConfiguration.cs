using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// Describes basic configuration object.
    /// </summary>
    public class SetupConfiguration
    {
        #region Xml element names
        /// <summary>
        /// Name of the document root element: this name is not relevant, we use just "Root"
        /// </summary>
        public static readonly XName xDocumentRootName = XNamespace.None + "Root";

        /// <summary>
        /// Name of the root CKSetup element. This must be the first element in the xml document,
        /// the second one being the engine configuration that becomes the root of runner configuration file
        /// (its name doesn't matter).
        /// A new CKSetup element is injected at the end of this root of runner configuration file that
        /// contains the <see cref="xEngineAssemblyQualifiedName"/> element and <see cref="xBinPaths"/>.
        /// </summary>
        public static readonly XName xCKSetup = XNamespace.None + "CKSetup";

        /// <summary>
        /// Name of the BaseName.
        /// This element also appears in the runner configuration file's CKSetup element.
        /// </summary>
        public static readonly XName xBasePath = XNamespace.None + "BasePath";

        /// <summary>
        /// Name of the EngineAssemblyQualifiedName.
        /// This element also appears in the runner configuration file's CKSetup element.
        /// </summary>
        public static readonly XName xEngineAssemblyQualifiedName = XNamespace.None + "EngineAssemblyQualifiedName";

        /// <summary>
        /// Name of the BinPaths element in <see cref="xCKSetup"/>.
        /// This element also appears in the runner configuration file's CKSetup element but its <see cref="xBinPath"/>
        /// children are different..
        /// </summary>
        public static readonly XName xBinPaths = XNamespace.None + "BinPaths";

        /// <summary>
        /// Name of the <see cref="ExternalSignature"/> element.
        /// </summary>
        static readonly XName xExternalSignature = XNamespace.None + "ExternalSignature";

        /// <summary>
        /// Name of a path to consider as a source for setup.
        /// This element also appears in the runner configuration file's CKSetup/BinPaths element
        /// where they have a BinPath attribute with the normalized full path of the folder
        /// and <see cref="xModel"/>, <see cref="xDependency"/> and <see cref="xModelDependent"/>
        /// children elements that contain an assembly name.
        /// </summary>
        public static readonly XName xBinPath = XNamespace.None + "BinPath";

        /// <summary>
        /// This element appears in the runner configuration file's CKSetup/RunSignature element.
        /// Its value is the current signature and may typically be injected as a <see cref="FileVersionInfo"/>
        /// property of generated files.
        /// </summary>
        public static readonly XName xRunSignature = XNamespace.None + "RunSignature";

        /// <summary>
        /// This element appears in the runner configuration file's CKSetup/BinPaths/BinPath elements.
        /// Its value is an assembly name that is a Model.
        /// </summary>
        public static readonly XName xModel = XNamespace.None + "Model";

        /// <summary>
        /// This element appears in the runner configuration file's CKSetup/BinPaths/BinPath elements.
        /// Its value is an assembly name that is a Dependency.
        /// </summary>
        public static readonly XName xDependency = XNamespace.None + "Dependency";

        /// <summary>
        /// Optional ordered set of <see cref="TargetRuntime"/> that will be selected
        /// if possible. First is better.
        /// It is a semicolon separated string of target runtimes: Net461;NetCoreApp20;NetCoreApp21.
        /// </summary>
        public static readonly XName xPreferredTargetRuntimes = XNamespace.None + "PreferredTargetRuntimes";

        /// <summary>
        /// This element appears in the runner configuration file's CKSetup/BinPaths/BinPath elements.
        /// Its value is an assembly name that depends on a Model.
        /// </summary>
        public static readonly XName xModelDependent = XNamespace.None + "ModelDependent";

        static readonly XName xUseSignatureFiles = XNamespace.None + "UseSignatureFiles";
        static readonly XName xWorkingDirectory = XNamespace.None + "WorkingDirectory";
        static readonly XName xDependencies = XNamespace.None + "Dependencies";
        static readonly XName xConfigurationDefaultName = XNamespace.None + "Configuration";
        #endregion

        XElement _configuration;
        string _externalSignature;

        /// <summary>
        /// Initializes a new empty configuration.
        /// </summary>
        public SetupConfiguration()
        {
            BinPaths = new List<string>();
            Dependencies = new List<SetupDependency>();
            _configuration = new XElement( xConfigurationDefaultName );
            UseSignatureFiles = true;
            PreferredTargetRuntimes = new List<TargetRuntime>();
            _externalSignature = String.Empty;
        }

        /// <summary>
        /// Initializes a new configuration from a xml document.
        /// The document must have exactly one CKSetup with valid elements and another element (the <see cref="Configuration"/>)
        /// otherwise an exception is thrown.
        /// </summary>
        /// <param name="d">A valid xml document.</param>
        public SetupConfiguration( XDocument d )
        {
            var elements = d.Root.Elements().ToList();
            if( elements.Count != 2 ) throw new ArgumentException( $"Xml document must have 2 and only 2 elements (found {elements.Count})." );
            var ckSetup = elements[0];
            if( ckSetup.Name != xCKSetup ) throw new ArgumentException( $"First element name must be '{xCKSetup}', not '{ckSetup.Name}')." );

            WorkingDirectory = ckSetup.Element( xWorkingDirectory )?.Value;

            BasePath = ckSetup.Element( xBasePath )?.Value;

            UseSignatureFiles = !string.Equals( ckSetup.Element( xUseSignatureFiles )?.Value, "false", StringComparison.OrdinalIgnoreCase );

            BinPaths = ckSetup.Elements( xBinPaths )
                                .Elements( xBinPath )
                                .Select( e => e.Value )
                                .ToList();

            PreferredTargetRuntimes = (ckSetup.Element( xPreferredTargetRuntimes )?.Value ?? String.Empty)
                                        .Split( ';' )
                                        .Select( x => x.Trim() )
                                        .Where( x => x.Length > 0 )
                                        .Select( x => (TargetRuntime)Enum.Parse( typeof( TargetRuntime ), x, ignoreCase:true ) )
                                        .ToList();

            Dependencies = ckSetup.Elements( xDependencies )
                                    .Elements( SetupDependency.xDependency )
                                    .Select( e => new SetupDependency( e ) )
                                    .ToList();

            ExternalSignature = ckSetup.Element( xExternalSignature )?.Value;

            var engine = ckSetup.Elements( xEngineAssemblyQualifiedName );
            if( engine.Count() != 1 ) throw new ArgumentException( $"{xCKSetup} element must contain one and only one '{xEngineAssemblyQualifiedName}' element." );
            EngineAssemblyQualifiedName = engine.Single().Value;
            _configuration = elements[1];
        }

        /// <summary>
        /// Helper to load a configuration file.
        /// If no <see cref="xBasePath"/> element exists in the file, <paramref name="path"/>'s directory is set as the base path.
        /// </summary>
        /// <param name="path">The path of the xml file.</param>
        /// <returns>A configuration object.</returns>
        public static SetupConfiguration Load( string path )
        {
            path = System.IO.Path.GetFullPath( path );
            var c = new SetupConfiguration( XDocument.Load( path ) );
            if( String.IsNullOrWhiteSpace( c.BasePath ) ) c.BasePath = System.IO.Path.GetDirectoryName( path ); 
            return c;
        }

        /// <summary>
        /// Creates a xml document from this configuration.
        /// </summary>
        /// <param name="forSignature">True to skip <see cref="UseSignatureFiles"/> that should not be used when computing signatures :).</param>
        /// <returns>The Xml document.</returns>
        public XDocument ToXml( bool forSignature = false )
        {
            return new XDocument( new XElement( xDocumentRootName,
                                    new XElement( xCKSetup,
                                        !string.IsNullOrWhiteSpace( BasePath )
                                            ? new XElement( xBasePath, BasePath )
                                            : null,
                                        !string.IsNullOrWhiteSpace( WorkingDirectory )
                                            ? new XElement( xWorkingDirectory, WorkingDirectory )
                                            : null,
                                        new XElement( xPreferredTargetRuntimes, String.Join(";", PreferredTargetRuntimes.Select( x => x.ToString() ) ) ),    
                                        !(forSignature || UseSignatureFiles) ? new XElement( xUseSignatureFiles, false ) : null,
                                        new XElement( xBinPaths, BinPaths.Select( p => new XElement( xBinPath, p ) ) ),
                                        new XElement( xDependencies, Dependencies.Select( p => p.ToXml() ) ),
                                        new XElement( xEngineAssemblyQualifiedName, EngineAssemblyQualifiedName ),
                                        ExternalSignature.Length > 0 ? new XElement( xExternalSignature, ExternalSignature ) : null ),
                                    Configuration ) );
        }

        /// <summary>
        /// Gets or sets a base path that applies to relative <see cref="BinPaths"/> and may be used
        /// at the runner level.
        /// When null or empty, the current directory is used.
        /// When <see cref="Load"/> is used and the file does not contain <see cref="xBasePath"/>, this
        /// base path is the file's directory.
        /// </summary>
        public string BasePath { get; set; }


        /// <summary>
        /// Gets or sets the working directory.
        /// When null or empty, a temporary folder is used.
        /// </summary>
        public string WorkingDirectory { get; set; }

        /// <summary>
        /// Gets or sets whether signature files should be used to skip the setup based
        /// on the files, their length and last write times and the properties of this SetupConfiguration.
        /// Defaults to true.
        /// <para>
        /// Signature files are always generated in <see cref="BinPaths"/> et the end of a successful setup. 
        /// </para>
        /// </summary>
        public bool UseSignatureFiles { get; set; }

        /// <summary>
        /// Gets a list of setup dependencies.
        /// </summary>
        public List<SetupDependency> Dependencies { get; }

        /// <summary>
        /// Gets or sets a string that can be anything: it participates to
        /// signature computation and enable callers to sign a setup with any external information
        /// by concatenating any string to it.
        /// Defaults to the empty string.
        /// </summary>
        public string ExternalSignature
        {
            get => _externalSignature;
            set => _externalSignature = value ?? String.Empty;
        }

        /// <summary>
        /// Optional ordered set of <see cref="TargetRuntime"/> that will be selected
        /// if possible. First is better.
        /// In Xml, it is a semicolon separated string of target runtimes:
        /// Net461;NetCoreApp20;NetCoreApp21.
        /// </summary>
        public List<TargetRuntime> PreferredTargetRuntimes { get; }

        /// <summary>
        /// Gets a list of binary paths to setup.
        /// It must not be empty, all paths must be valid.
        /// When the same assembly belongs to different folders, it must
        /// be exactly the same file.
        /// </summary>
        public List<string> BinPaths { get; }

        /// <summary>
        /// Assembly qualified name of the engine type.
        /// It can be any object with a public constructor with two parameters
        /// (a IActivityMonitor and a <see cref="XElement"/>) and
        /// a public <c>bool Run()</c> method.
        /// <para>
        /// Example: "CK.Setup.StObjEngine, CK.StObj.Engine"
        /// </para>
        /// </summary>
        public string EngineAssemblyQualifiedName { get; set; }

        /// <summary>
        /// Gets or sets the configuration that will be provided to the engine.
        /// It must not be null and can have any name (defaults to "Configuration").
        /// </summary>
        public XElement Configuration
        {
            get => _configuration;
            set => _configuration = value ?? throw new ArgumentNullException();
        }
    }
}
