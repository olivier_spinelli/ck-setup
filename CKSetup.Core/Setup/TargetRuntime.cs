using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup
{
    /// <summary>
    /// Describes target runtime.
    /// See <see cref="TargetRuntimeOrFrameworkExtension"/>.
    /// </summary>
    public enum TargetRuntime
    {
        /// <summary>
        /// Not applicable.
        /// </summary>
        None = 0,

        /// <summary>
        /// Net461 runtime.
        /// </summary>
        Net461 = 1 << 16,

        /// <summary>
        /// Net462 runtime.
        /// </summary>
        Net462 = 2 << 16,

        /// <summary>
        /// Net47 runtime.
        /// </summary>
        Net47 = 3 << 16,

        /// <summary>
        /// Net471 runtime.
        /// </summary>
        Net471 = 4 << 16,

        /// <summary>
        /// Net472 runtime.
        /// </summary>
        Net472 = 5 << 16,

        /// <summary>
        /// NetCoreApp1.0 runtime.
        /// </summary>
        NetCoreApp10 = 128 << 16,

        /// <summary>
        /// NetCoreApp1.1 runtime.
        /// </summary>
        NetCoreApp11 = 129 << 16,

        /// <summary>
        /// NetCoreApp2.0 runtime.
        /// </summary>
        NetCoreApp20 = 130 << 16,

        /// <summary>
        /// NetCoreApp2.1 runtime.
        /// </summary>
        NetCoreApp21 = 131 << 16,
    }

}
