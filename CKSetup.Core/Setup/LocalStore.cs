using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK.Core;
using System.Xml.Linq;
using CSemVer;
using CK.Text;
using System.Diagnostics;
using System.IO;
using CKSetup.StreamStore;
using System.Net.Http;

namespace CKSetup
{
    /// <summary>
    /// Primary object that encapsulates and coordinates a <see cref="IStreamStore"/> and a <see cref="ComponentDB"/>.
    /// It must be disposed once useless.
    /// </summary>
    public partial class LocalStore : IDisposable
    {
        /// <summary>
        /// The component file.
        /// </summary>
        public const string DbXmlFileName = "db.xml";
        readonly IStreamStore _store;
        readonly List<string> _cleanupFiles;
        readonly IActivityMonitor _monitor;
        readonly CompressionKind _storageKind;
        HttpClient _httpClient;
        IRemoteStore _prototypeStore;
        ComponentDB _dbCurrent;
        ComponentDB _dbOrigin;
        DateTime _lastWriteTimeDB;

        /// <summary>
        /// Initializes a new <see cref="LocalStore"/>.
        /// </summary>
        /// <param name="monitor">The monitor that is captured and be used by this <see cref="LocalStore"/>.</param>
        /// <param name="store">The stream store.</param>
        /// <param name="storageKind">The compression mode of the store.</param>
        public LocalStore( IActivityMonitor monitor, IStreamStore store, CompressionKind storageKind )
        {
            _store = store;
            _cleanupFiles = new List<string>();
            _monitor = monitor;
            _storageKind = storageKind;
            Tuple<ComponentDB, DateTime> read = _store.Initialize( monitor );
            _dbOrigin = _dbCurrent = read.Item1;
            if( _dbOrigin == null )
            {
                _store.Dispose();
            }
            else _lastWriteTimeDB = read.Item2;
        }

        /// <summary>
        /// Gets whether this database has been successfully opened.
        /// </summary>
        public bool IsValid => _dbCurrent != null;

        /// <summary>
        /// Registers a file that will be automatically deleted when this <see cref="LocalStore"/>
        /// will be disposed.
        /// </summary>
        /// <param name="fullPath"></param>
        public void RegisterFileToDelete( string fullPath )
        {
            _cleanupFiles.Add( fullPath );
        }

        /// <summary>
        /// Removes all registered components: the component database becomes <see cref="ComponentDB.Empty"/>
        /// and all stored files are deleted.
        /// </summary>
        /// <returns>True on success, false on error.</returns>
        public bool Clear()
        {
            using( _monitor.OpenInfo( $"Clearing file store." ) )
            {
                try
                {
                    _dbCurrent = ComponentDB.Empty;
                    int deleted = _store.Delete( f => !f.Equals( DbXmlFileName, StringComparison.OrdinalIgnoreCase ) );
                    _monitor.CloseGroup( $"{deleted} entries removed." );
                    SaveDbCurrent();
                    return true;
                }
                catch( Exception ex )
                {
                    _monitor.Error( ex );
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets or sets the prototype store url. Null if not specified.
        /// When specified, this store is used a fallback to resolve missing components and files.
        /// </summary>
        public Uri PrototypeStoreUrl
        {
            get => _dbCurrent.PrototypeStoreUrl;
            set
            {
                if( value != _dbCurrent.PrototypeStoreUrl )
                {
                    _dbCurrent = _dbCurrent.WithPrototypeStoreUrl( value );
                    if( _prototypeStore != null )
                    {
                        _prototypeStore.Dispose();
                        _prototypeStore = null;
                    }
                    SaveDbCurrent();
                }
            }
        }

        internal IComponentImporter GetPrototypeStore( IActivityMonitor monitor )
        {
            if( _prototypeStore == null && _dbCurrent.PrototypeStoreUrl != null )
            {
                _prototypeStore = ClientRemoteStore.Create( monitor, _dbCurrent.PrototypeStoreUrl, null, GetHttpClient );
                if( _prototypeStore == null ) monitor.Warn( $"Unable to create a client for prototype store '{_dbCurrent.PrototypeStoreUrl}'." );
            }
            return _prototypeStore;
        }

        HttpClient GetHttpClient() => _httpClient ?? (_httpClient = new HttpClient());

        /// <summary>
        /// Removes components that match a predicate.
        /// This does not remove any potential dependencies (a <see cref="ComponentDependency"/>
        /// only defines the name of its target and a minimal version).
        /// </summary>
        /// <param name="exclude">A predicate that must return true to remove the component.</param>
        /// <returns>The number of removed components.</returns>
        public int RemoveComponents( Func<Component, bool> exclude )
        {
            int originalCount = _dbCurrent.Components.Count;
            _dbCurrent = _dbCurrent.RemoveComponents( _monitor, exclude );
            SaveDbCurrent();
            return originalCount - _dbCurrent.Components.Count;
        }

        /// <summary>
        /// Deletes all files in the file store that are no more referenced by any existing components.
        /// This operation may take a long time (depending of the file numbers).
        /// </summary>
        /// <returns>The number of files deleted or -1 if an error occurred.</returns>
        public int GarbageCollectFiles()
        {
            using( _monitor.OpenInfo( $"Garbage collecting of file store." ) )
            {
                try
                {
                    var fileNames = new HashSet<string>( _dbCurrent.Components.SelectMany( c => c.Files ).Select( f => f.SHA1.ToString() ), StringComparer.OrdinalIgnoreCase );
                    fileNames.Add( DbXmlFileName );
                    int deleted = _store.Delete( f => !fileNames.Contains( f ) );
                    _monitor.CloseGroup( $"{deleted} files deleted." );
                    return deleted;
                }
                catch( Exception ex )
                {
                    _monitor.Error( ex );
                    return -1;
                }
            }
        }

        /// <summary>
        /// Determines whether a component is registered.
        /// </summary>
        /// <param name="n">Component name.</param>
        /// <param name="t">Target framework of the component.</param>
        /// <param name="v">Version of the component.</param>
        public bool Contains( string n, TargetFramework t, SVersion v )
        {
            return _dbCurrent.Find( new ComponentRef( n, t, v ) ) != null;
        }

        /// <summary>
        /// Intermediate state that handles import.
        /// </summary>
        public class LocalImporter
        {
            readonly LocalStore _store;
            readonly HashSet<BinFolder> _toAdd;
            readonly IComponentImporter _missingImporter;

            internal LocalImporter( LocalStore a, IComponentImporter missingImporter )
            {
                _store = a;
                _toAdd = new HashSet<BinFolder>();
                _missingImporter = missingImporter;
            }

            IActivityMonitor Monitor => _store._monitor;

            /// <summary>
            /// Adds one or more components. Ignores them when already registered.
            /// </summary>
            /// <param name="folder">The (potentially multiples) component folder.</param>
            /// <returns>This importer (enable fluent syntax).</returns>
            public LocalImporter AddComponent( params BinFolder[] folder ) => AddComponent( (IEnumerable<BinFolder>)folder );

            /// <summary>
            /// Adds one or more components. Ignores them when already registered.
            /// </summary>
            /// <param name="folders">Folders to add. Must not contain null folders (<see cref="Import"/> will fail).</param>
            /// <returns>This importer (enable fluent syntax).</returns>
            public LocalImporter AddComponent( IEnumerable<BinFolder> folders )
            {
                if( folders == null ) throw new ArgumentNullException( nameof( folders ) );
                _toAdd.AddRange( folders );
                return this;
            }

            /// <summary>
            /// Tries to import added component.
            /// </summary>
            /// <returns>True on success, false on error.</returns>
            public bool Import()
            {
                using( _store._monitor.OpenInfo( $"Importing {_toAdd.Count} local folders." ) )
                {
                    if( _toAdd.Count == 0 ) return true;
                    if( _toAdd.Any( f => f == null ) )
                    {
                        _store._monitor.Error( $"Added BinFolder at index {_toAdd.IndexOf( f => f == null )} is null." );
                        return false;
                    }
                    // Small trick: ordering by files count will tend to reduce
                    // Components mutation and multiple heads registration errors
                    // since smallest (ie. most basic) components come first.
                    return _store.LocalImport( _toAdd.OrderBy( f => f.Files.Count ), _missingImporter );
                }
            }
        }

        /// <summary>
        /// Creates a local importer object that can be used to import local <see cref="BinFolder"/>.
        /// </summary>
        /// <param name="missingImporter">
        /// Optional importer that should be provided in order to resolve embedded components
        /// that may not already be in this local store.
        /// </param>
        /// <returns>An importer.</returns>
        public LocalImporter CreateLocalImporter( IComponentImporter missingImporter = null )
        {
            if( !IsValid ) throw new InvalidOperationException();
            return new LocalImporter( this, missingImporter );
        }

        bool LocalImport( IOrderedEnumerable<BinFolder> folders, IComponentImporter missingImporter )
        {
            if( !IsValid ) throw new InvalidOperationException();
            ComponentDB db = _dbCurrent;
            var added = new Dictionary<ComponentRef, BinFolder>();
            foreach( var f in folders )
            {
                using( _monitor.OpenInfo( $"Adding {f.BinPath}." ) )
                {
                    var result = db.AddLocal( _monitor, f );
                    if( result.Error ) return false;
                    // We do not store files for models in .Net framework: they are necessarily in 
                    // any target folders that has the model!
                    if( result.NewComponent != null && result.NewComponent.StoreFiles )
                    {
                        added[result.NewComponent.GetRef()] = f;
                    }
                    db = result.NewDB;
                }
            }
            var newC = added.Select( kv => new { C = db.Components.Single( c => c.GetRef().Equals( kv.Key ) ), F = kv.Value } );
            if( newC.Any( c => c.C.Embedded.Count > 0 ) )
            {
                if( missingImporter != null || GetPrototypeStore( _monitor ) != null )
                {
                    using( _monitor.OpenInfo( $"Components have missing embedded components. Trying to use importer." ) )
                    {
                        var downloader = new ComponentDownloader( missingImporter, db, _store, _storageKind, GetPrototypeStore );
                        var missing = new ComponentMissingDescription( newC.SelectMany( c => c.C.Embedded ).ToList() );
                        var updatedDB = downloader.Download( _monitor, missing );
                        if( updatedDB == null ) return false;
                        db = updatedDB;
                    }
                }
                else
                {
                    using( _monitor.OpenError( "Components have missing embedded components and no remote has been specified. Embedded components are required to be resolved." ) )
                    {
                        foreach( var c in newC.Where( c => c.C.Embedded.Count > 0 ) )
                        {
                            _monitor.Trace( $"'{c.C}' embedds: '{c.C.Embedded.Select( e => e.ToString() ).Concatenate( "', '" )}'." );
                        }
                    }
                    return false;
                }
            }
            foreach( var c in newC )
            {
                foreach( var f in c.C.Files )
                {
                    using( _monitor.OpenTrace( $"Importing file '{f}'." ) )
                    {
                        try
                        {
                            string key = f.SHA1.ToString();
                            if( _store.Exists( key ) )
                            {
                                _monitor.CloseGroup( "Already stored." );
                            }
                            else
                            {
                                using( var content = File.OpenRead( c.F.Files.First( b => b.ContentSHA1 == f.SHA1 ).FullPath ) )
                                {
                                    _store.Create( key, content, CompressionKind.None, _storageKind );
                                }
                            }
                        }
                        catch( Exception ex )
                        {
                            _monitor.Error( ex );
                            return false;
                        }
                    }
                }
            }
            _dbCurrent = db;
            SaveDbCurrent();
            return true;
        }

        /// <summary>
        /// Internal implementation of <see cref="IComponentDownloader"/>.
        /// It uses a <see cref="IComponentImporter"/> importer to update a local store
        /// with as much components and files that it can.
        /// </summary>
        class ComponentDownloader : IComponentDownloader
        {
            readonly IComponentImporter _importer;
            readonly IStreamStore _store;
            readonly CompressionKind _storageKind;
            readonly Func<IActivityMonitor, IComponentImporter> _prototypeStoreAccessor;
            ComponentDB _db;

            public ComponentDownloader( IComponentImporter importer, LocalStore a )
                : this( importer, a._dbCurrent, a._store, a._storageKind, a.GetPrototypeStore )
            {
            }

            public ComponentDownloader(
                IComponentImporter importer,
                ComponentDB db,
                IStreamStore store,
                CompressionKind storageKind,
                Func<IActivityMonitor, IComponentImporter> prototypeStore )
            {
                _importer = importer;
                _store = store;
                _storageKind = storageKind;
                _db = db;
                _prototypeStoreAccessor = prototypeStore;
            }

            public ComponentDB Download( IActivityMonitor monitor, ComponentMissingDescription missing )
            {
                IComponentImporter store1, store2;

                IDisposable monitorGroup = null;
                IComponentImporter prototypeStore = _prototypeStoreAccessor?.Invoke( monitor );
                if( prototypeStore != null )
                {
                    store1 = prototypeStore;
                    if( _importer != null )
                    {
                        if( _importer.Url != prototypeStore.Url )
                        {
                            store2 = _importer;
                            monitorGroup = monitor.OpenInfo( $"Using prototype store ({prototypeStore.Url}) first and then specified remote store ({_importer.Url}) if needed." );
                        }
                        else
                        {
                            store2 = null;
                            monitorGroup = monitor.OpenInfo( $"Using prototype store that is the same as specified remote store ({prototypeStore.Url})" );
                        }
                    }
                    else
                    {
                        store2 = null;
                        monitorGroup = monitor.OpenInfo( $"No remote store has been specified. Using prototype store only ({prototypeStore.Url})." );
                    }
                }
                else if( _importer != null )
                {
                    store1 = _importer;
                    store2 = null;
                    monitorGroup = monitor.OpenInfo( $"No prototype store. Using specified remote store ({_importer.Url})." );
                }
                else
                {
                    monitor.Warn( $"No prototype store nor remote store specified. Unable to download components." );
                    return _db;
                }
                try
                {
                    return Download( monitor, missing, store1, store2 );
                }
                finally
                {
                    monitorGroup.Dispose();
                }
            }

            ComponentDB Download( IActivityMonitor monitor, ComponentMissingDescription missing, IComponentImporter store1, IComponentImporter store2 )
            {
                Debug.Assert( store1 != null );
                Debug.Assert( store2 == null || store1.Url != store2.Url );
                ComponentDB db = DoDownload( monitor, missing, store1 );
                if( db != null && store2 != null )
                {
                    var unresolved = missing.Components.Where( c => db.Find( c ) == null ).ToList();
                    var notPerfect = missing.Dependencies
                                            .Select( d => new { Dep = d, Best = db.FindBest( missing.TargetRuntime, d.UseName, d.UseMinVersion ) } )
                                            // The dependency has not been imported at all...
                                            // ...or there is a MinVersion and we found a greater version.
                                            //    (When there is no MinVersion requirement, we keep what the remote sent us.)
                                            // ...or the component has a perfect runtime and we did not obtain it.
                                            .Where( t => t.Best == null
                                                         || (t.Dep.UseMinVersion != null && t.Best.Version != t.Dep.UseMinVersion) 
                                                         || (t.Best.TargetFramework.GetSinglePerfectRuntime() != TargetRuntime.None
                                                             && t.Best.TargetFramework.GetSinglePerfectRuntime() != missing.TargetRuntime) )
                                            .Select( t => t.Dep ).ToList();
                    if( unresolved.Count > 0 || notPerfect.Count > 0 )
                    {
                        var stillMissing = new ComponentMissingDescription( missing.TargetRuntime, notPerfect, unresolved );
                        db = DoDownload( monitor, stillMissing, store2 );
                    }
                }
                return db;
            }

            ComponentDB DoDownload( IActivityMonitor monitor, ComponentMissingDescription missing, IComponentImporter store )
            {
                using( monitor.OpenTrace( $"Download from store: {store.Url}." ) )
                {
                    using( Stream s = store.OpenImportStream( monitor, missing ) )
                    {
                        if( s == null || !ImportComponents( monitor, s, store ) ) return null;
                    }
                }
                return _db;
            }

            bool ImportComponents( IActivityMonitor monitor, Stream input, IComponentFileDownloader downloader )
            {
                using( monitor.OpenInfo( $"Starting import from {downloader.Url}." ) )
                {
                    var importResult = _db.Import( monitor, input );
                    if( importResult.Error ) return false;
                    var r = _store.DownloadImportResult( monitor, downloader, importResult, _storageKind );
                    if( r.Item2 > 0 ) return false;
                    _db = importResult.NewDB;
                    return true;
                }
            }
        }

        /// <summary>
        /// Captures result of <see cref="ExtractRuntimeDependencies"/>.
        /// </summary>
        public struct ExtractRuntimeDependenciesResult
        {
            readonly DependencyResolver.RunResult _depsResult;

            internal ExtractRuntimeDependenciesResult( DependencyResolver.RunResult r )
            {
                _depsResult = r;
            }

            /// <summary>
            /// Gets whether the extraction succeeded.
            /// </summary>
            public bool Success => _depsResult.Success;

            /// <summary>
            /// Gets the target final runtime.
            /// <see cref="TargetRuntime.None"/> if <see cref="Success"/> is false.
            /// </summary>
            public TargetRuntime TargetRuntime => _depsResult.Origin.TargetRuntime;

            /// <summary>
            /// Gets the list of <see cref="BinFolder"/>.
            /// Null if <see cref="Success"/> is false.
            /// </summary>
            public IReadOnlyList<BinFolder> Targets => _depsResult.Origin.Targets;

            /// <summary>
            /// Gets the Models found among the <see cref="Targets"/>.
            /// Null if <see cref="Success"/> is false.
            /// </summary>
            public IReadOnlyList<BinFileAssemblyInfo> Models => _depsResult.Origin.Models;

            /// <summary>
            /// Gets the ModelDependents assemblies found among the <see cref="Targets"/>.
            /// Null if <see cref="Success"/> is false.
            /// </summary>
            public IReadOnlyList<BinFileAssemblyInfo> ModelDependents => _depsResult.Origin.ModelDependents;

        }

        /// <summary>
        /// Extracts required runtime support for Models in targets.
        /// </summary>
        /// <param name="workingDirectory">The working directory.</param>
        /// <param name="targets">The target folders.</param>
        /// <param name="missingImporter">Component importer. Can be null.</param>
        /// <param name="explicitDependencies">Optional extra dependencies that, when specified, must be resolved.</param>
        /// <param name="preferredTargetRuntimes">
        /// Optional ordered set of <see cref="TargetRuntime"/> that will be selected
        /// if possible. First is better.
        /// </param>
        /// <returns>Result encapsulation.</returns>
        public ExtractRuntimeDependenciesResult ExtractRuntimeDependencies(
            string workingDirectory,
            IEnumerable<BinFolder> targets,
            IComponentImporter missingImporter,
            IEnumerable<SetupDependency> explicitDependencies,
            IEnumerable<TargetRuntime> preferredTargetRuntimes = null )
        {
            if( String.IsNullOrWhiteSpace( workingDirectory )
                || !Path.IsPathRooted( workingDirectory )
                || !Directory.Exists( workingDirectory )
                || workingDirectory[workingDirectory.Length - 1] != Path.DirectorySeparatorChar )
            {
                throw new ArgumentException( "WorkingDirectory must exist and end with a directory separator.", nameof( workingDirectory ) );
            }
            if( !targets.Any() ) throw new ArgumentException( "At least one target is required.", nameof( targets ) );

            DependencyResolver.RunResult resolveResult;
            ComponentFileCollector fileCollector;
            using( _monitor.OpenInfo( $"Resolving runtime support for '{targets.Select( t => t.BinPath ).Concatenate()}'." ) )
            {
                var resolver = _dbCurrent.GetRuntimeDependenciesResolver( _monitor, targets, explicitDependencies, preferredTargetRuntimes );
                if( resolver == null || resolver.IsEmpty ) return new ExtractRuntimeDependenciesResult();
                IComponentDownloader downloader = new ComponentDownloader( missingImporter, this );
                resolveResult = resolver.Run( _monitor, downloader );
                if( !resolveResult.Success ) return new ExtractRuntimeDependenciesResult();

                fileCollector = new ComponentFileCollector();
                fileCollector.Add( resolveResult.Components.OrderByDescending( x => x.TargetFramework ) );
                fileCollector.DumpResult( _monitor );
            }
            using( _monitor.OpenInfo( $"Copying files to '{workingDirectory}'." ) )
            {
                foreach( var f in fileCollector.Result )
                {
                    string targetPath = workingDirectory + f.Name;
                    try
                    {
                        string fileKey = f.SHA1.ToString();
                        if( !_store.Exists( fileKey ) )
                        {
                            if( missingImporter == null )
                            {
                                _monitor.Error( $"Missing file '{f}' in local store." );
                                return new ExtractRuntimeDependenciesResult();
                            }
                            if( !_store.Download( _monitor, missingImporter, f, _storageKind ) ) return new ExtractRuntimeDependenciesResult();
                        }
                        _store.ExtractToFile( fileKey, targetPath );
                        _monitor.Debug( $"Extracted {f.Name}." );
                    }
                    catch( Exception ex )
                    {
                        _monitor.Error( $"While extracting '{f.Name}'.", ex );
                        return new ExtractRuntimeDependenciesResult();
                    }
                }
            }
            if( _dbCurrent != resolveResult.FinalDB )
            {
                _monitor.Info( $"Store changed: { resolveResult.FinalDB.Components.Count - _dbCurrent.Components.Count} new components." );
                _dbCurrent = resolveResult.FinalDB;
            }
            return new ExtractRuntimeDependenciesResult( resolveResult );
        }

        /// <summary>
        /// Exports available components.
        /// </summary>
        /// <param name="what">Required description.</param>
        /// <param name="output">Output stream.</param>
        /// <param name="monitor">Monitor to use.</param>
        public void ExportComponents(
            ComponentMissingDescription what,
            Stream output,
            IActivityMonitor monitor )
        {
            var locallyAvailable = _dbCurrent.FindAvailable( monitor, what );
            if( locallyAvailable.ShouldBeCompleted && GetPrototypeStore( monitor ) != null )
            {
                using( monitor.OpenInfo( "Requesting Prototype store to complete the export." ) )
                {
                    var m = new ComponentMissingDescription( what.TargetRuntime, locallyAvailable.NotPerfectDependencies, locallyAvailable.Unresolved );
                    var newDB = new ComponentDownloader( null, this ).Download( monitor, m );
                    if( newDB != null && newDB != _dbCurrent )
                    {
                        _dbCurrent = newDB;
                        SaveDbCurrent();
                        locallyAvailable = _dbCurrent.FindAvailable( monitor, what );
                    }
                }
            }
            _dbCurrent.Export( c => locallyAvailable.Resolved.Contains( c ), output );
        }

        /// <summary>
        /// Imports a set of components from a <see cref="Stream"/> and a downloader.
        /// </summary>
        /// <param name="input">Input stream.</param>
        /// <param name="downloader">Missing files downloader.</param>
        /// <returns>True on success, false on error.</returns>
        public bool ImportComponents( Stream input, IComponentFileDownloader downloader )
        {
            if( downloader == null ) throw new ArgumentNullException( nameof( downloader ) );
            using( _monitor.OpenInfo( "Starting import with file downloader." ) )
            {
                var n = _dbCurrent.Import( _monitor, input );
                if( n.Error ) return false;
                var r = _store.DownloadImportResult( _monitor, downloader, n, _storageKind );
                if( r.Item2 > 0 )
                {
                    _monitor.Error( $"{r.Item2} download errors. Import canceled." );
                    return false;
                }
                _dbCurrent = n.NewDB;
                SaveDbCurrent();
                return true;
            }
        }

        /// <summary>
        /// Imports a set of components from a <see cref="Stream"/> and returns
        /// a <see cref="PushComponentsResult"/>.
        /// </summary>
        /// <param name="input">Input stream.</param>
        /// <param name="sessionId">
        /// Optional session identifier.
        /// When not set, <see cref="PushComponentsResult.SessionId"/> is null on error
        /// and a new guid is generated on success.
        /// </param>
        /// <returns>True on success, false on error.</returns>
        public PushComponentsResult ImportComponents( Stream input, string sessionId = null )
        {
            using( _monitor.OpenInfo( "Starting import." ) )
            {
                var n = _dbCurrent.Import( _monitor, input );
                if( n.Error ) return new PushComponentsResult( "Error while importing component into ComponentDB.", sessionId );
                IReadOnlyList<SHA1Value> missingFiles;
                if( n.Components != null && n.Components.Count > 0 )
                {
                    missingFiles = n.Components
                                    .Where( c => c.StoreFiles )
                                    .SelectMany( c => c.Files )
                                    .Select( f => f.SHA1 )
                                    .Distinct()
                                    .Where( sha => !_store.Exists( sha.ToString() ) )
                                    .ToList();
                }
                else missingFiles = Array.Empty<SHA1Value>();
                _dbCurrent = n.NewDB;
                SaveDbCurrent();
                return new PushComponentsResult( missingFiles, sessionId ?? Guid.NewGuid().ToString() );
            }
        }

        /// <summary>
        /// Pushes selected components to a <see cref="IComponentPushTarget"/>.
        /// </summary>
        /// <param name="filter">Filter for components to export.</param>
        /// <param name="target">Target for the components.</param>
        /// <returns>True on success, false otherwise.</returns>
        public bool PushComponents( Func<Component, bool> filter, IComponentPushTarget target )
        {
            if( filter == null ) throw new ArgumentNullException( nameof( filter ) );
            if( target == null ) throw new ArgumentNullException( nameof( target ) );

            bool fileError = false;
            using( _monitor.OpenInfo( $"Starting component push." ) )
            {
                var result = target.PushComponents( _monitor, w => _dbCurrent.Export( filter, w ) );
                if( result.ErrorText != null )
                {
                    _monitor.Error( "Target error: " + result.ErrorText );
                    return false;
                }
                int fileCount = 0;
                if( result.Files.Count > 0 )
                {
                    using( _monitor.OpenInfo( $"Starting {result.Files.Count} push. SessionId={result.SessionId}." ) )
                    {
                        foreach( var sha in result.Files )
                        {
                            ++fileCount;
                            LocalStoredStream sf = _store.OpenRead( sha.ToString() );
                            if( sf.Stream != null )
                            {
                                try
                                {
                                    if( !target.PushFile( _monitor, result.SessionId, sha, w => sf.Stream.CopyTo( w ), sf.Kind ) )
                                    {
                                        _monitor.Error( $"Failed to push file {sha}." );
                                        --fileCount;
                                        fileError = true;
                                    }
                                }
                                finally
                                {
                                    sf.Stream.Dispose();
                                }
                            }
                            else
                            {
                                _monitor.Warn( $"Target requested file '{sha}' that does not locally exist." );
                                --fileCount;
                            }
                        }
                    }
                }
                if( !fileError ) _monitor.CloseGroup( $"Target is up to date. {fileCount} file uploaded." );
            }
            return !fileError;
        }


        /// <summary>
        /// Pushes selected components to a remote or local url.
        /// </summary>
        /// <param name="filter">Filter for components to export.</param>
        /// <param name="url">Url of the remote.</param>
        /// <param name="apiKey">Optional api key.</param>
        /// <returns>True on success, false otherwise.</returns>
        public bool PushComponents( Func<Component, bool> filter, Uri url, string apiKey )
        {
            if( filter == null ) throw new ArgumentNullException( nameof( filter ) );
            if( url == null ) throw new ArgumentNullException( nameof( url ) );
            using( var remote = ClientRemoteStore.Create( _monitor, url, apiKey ) )
            {
                return remote != null && PushComponents( filter, remote );
            }
        }

        void SaveDbCurrent()
        {
            if( _dbOrigin != _dbCurrent && _lastWriteTimeDB != Util.UtcMaxValue )
            {
                _lastWriteTimeDB = _store.Save( _dbCurrent, _lastWriteTimeDB );
                _dbOrigin = _dbCurrent;
            }
            if( _lastWriteTimeDB == Util.UtcMaxValue )
            {
                _monitor.Error( $"Concurrency error detected: store has been updated by another process. Changes to the component database from this one will be lost." );
            }
        }

        /// <summary>
        /// Closes this store. Files registered by <see cref="RegisterFileToDelete"/> 
        /// are deleted and updated component database is written in the zip if required.
        /// </summary>
        public void Dispose()
        {
            if( _dbCurrent == null ) return;
            using( _monitor.OpenInfo( "Closing Store." ) )
            {
                if( _cleanupFiles.Count > 0 )
                {
                    using( _monitor.OpenTrace( $"Cleaning {_cleanupFiles.Count} runtime files." ) )
                    {
                        foreach( var f in _cleanupFiles )
                        {
                            try
                            {
                                File.Delete( f );
                            }
                            catch( Exception ex )
                            {
                                _monitor.Warn( ex );
                            }
                        }
                        _cleanupFiles.Clear();
                    }
                }
                SaveDbCurrent();
                if( _httpClient != null ) _httpClient.Dispose();
                _httpClient = null;
                if( _prototypeStore != null ) _prototypeStore.Dispose();
                _prototypeStore = null;
                _store.Dispose();
                _dbCurrent = null;
            }
        }

        /// <summary>
        /// Opens an existing store (zip or directory).
        /// The .zip file or directory must exist otherwise null is returned.
        /// </summary>
        /// <param name="m">Monitor to use. Can not be null.</param>
        /// <param name="path">Path to the file or directory to open.</param>
        /// <returns>A store or null on error.</returns>
        static public LocalStore Open( IActivityMonitor m, string path )
        {
            if( path == null ) throw new ArgumentNullException( nameof( path ) );
            if( path.EndsWith( ".zip", StringComparison.OrdinalIgnoreCase ) )
            {
                if( !File.Exists( path ) )
                {
                    m.Error( $"Local store zip file not found: {path}" );
                    return null;
                }
            }
            else
            {
                if( !Directory.Exists( path ) )
                {
                    m.Error( $"Local store directory not found: '{path}'." );
                    return null;
                }
            }
            return OpenOrCreate( m, path );
        }

        /// <summary>
        /// Opens an existing store or creates a new one.
        /// Returns null on error.
        /// </summary>
        /// <param name="m">Monitor to use. Can not be null.</param>
        /// <param name="path">Path to the file or directory to open or create.</param>
        /// <returns>An store or null on error.</returns>
        static public LocalStore OpenOrCreate( IActivityMonitor m, string path )
        {
            try
            {
                IStreamStore store;
                CompressionKind kind;
                if( path.EndsWith( ".zip", StringComparison.OrdinalIgnoreCase ) )
                {
                    store = new ZipFileStreamStore( path );
                    kind = CompressionKind.None;
                }
                else
                {
                    store = new DirectoryStreamStore( path );
                    kind = CompressionKind.GZiped;
                }
                var a = new LocalStore( m, store, kind );
                if( !a.IsValid ) return null;
                m.Trace( $"Local store opened on : '{path}'." );
                return a;
            }
            catch( Exception ex )
            {
                m.Fatal( $"While opening or creating store '{path}'.", ex );
                return null;
            }
        }

    }
}
