using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup
{
    /// <summary>
    /// Extends <see cref="TargetFramework"/> and <see cref="TargetRuntime"/> with
    /// helpers.
    /// </summary>
    public static class TargetRuntimeOrFrameworkExtension
    {

        /// <summary>
        /// Gets the runtimes that are common to multiple frameworks.
        /// </summary>
        /// <param name="this">This framework.</param>
        /// <param name="others">Other frameworks.</param>
        /// <returns>The common runtimes.</returns>
        static public IEnumerable<TargetRuntime> GetCommonRuntimes( this TargetFramework @this, IEnumerable<TargetFramework> others )
        {
            var intersect = new HashSet<TargetRuntime>( @this.GetAllowedRuntimes() );
            foreach( var f in others )
            {
                intersect.IntersectWith( f.GetAllowedRuntimes() );
            }
            return intersect;
        }

        static TargetRuntime[] NetFramework461Runtimes = new TargetRuntime[] { TargetRuntime.Net461, TargetRuntime.Net462, TargetRuntime.Net47, TargetRuntime.Net471, TargetRuntime.Net472 };
        static TargetRuntime[] NetFramework462Runtimes = new TargetRuntime[] { TargetRuntime.Net462, TargetRuntime.Net47, TargetRuntime.Net471, TargetRuntime.Net472 };
        static TargetRuntime[] NetFramework47Runtimes = new TargetRuntime[] { TargetRuntime.Net47, TargetRuntime.Net471, TargetRuntime.Net472 };
        static TargetRuntime[] NetFramework471Runtimes = new TargetRuntime[] { TargetRuntime.Net471, TargetRuntime.Net472 };
        static TargetRuntime[] NetFramework472Runtimes = new TargetRuntime[] { TargetRuntime.Net472 };

        static TargetRuntime[] NetCoreApp11Runtimes = new TargetRuntime[] { TargetRuntime.NetCoreApp11, TargetRuntime.NetCoreApp20, TargetRuntime.NetCoreApp21 };
        static TargetRuntime[] NetCoreApp20Runtimes = new TargetRuntime[] { TargetRuntime.NetCoreApp20, TargetRuntime.NetCoreApp21 };
        static TargetRuntime[] NetCoreApp21Runtimes = new TargetRuntime[] { TargetRuntime.NetCoreApp21 };

        static TargetRuntime[] SupportedNetStandardRuntimes = NetCoreApp11Runtimes.Concat( NetFramework461Runtimes ).ToArray();
        //
        // NetStandard 1.0, 1.1, 1.2 and 1.3 are actually supported by older platfomrs (i.e. netstandard1.0 is net45 compliant).
        // However, we start support at Net461 and netcoreapp1.1: the following mappings are not accurate regarding old platforms
        // but we don't care about them.
        // See: https://docs.microsoft.com/en-us/dotnet/standard/net-standard#net-implementation-support
        //
        static TargetRuntime[] NetStandard10Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard11Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard12Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard13Runtimes = SupportedNetStandardRuntimes;
        // Those are accurate.
        static TargetRuntime[] NetStandard14Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard15Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard16Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard20Runtimes = SupportedNetStandardRuntimes;

        /// <summary>
        /// Gets the different <see cref="TargetRuntime"/> that can handle a <see cref="TargetFramework"/>.
        /// </summary>
        /// <param name="this">This framework.</param>
        /// <returns>The compatible runtimes.</returns>
        static public IReadOnlyList<TargetRuntime> GetAllowedRuntimes( this TargetFramework @this )
        {
            switch( @this )
            {
                case TargetFramework.Net451:
                case TargetFramework.Net46:
                case TargetFramework.Net461: return NetFramework461Runtimes;
                case TargetFramework.Net462: return NetFramework462Runtimes;
                case TargetFramework.Net47: return NetFramework47Runtimes;
                case TargetFramework.Net471: return NetFramework471Runtimes;
                case TargetFramework.Net472: return NetFramework472Runtimes;
                case TargetFramework.NetStandard10: return NetStandard10Runtimes;
                case TargetFramework.NetStandard11: return NetStandard11Runtimes;
                case TargetFramework.NetStandard12: return NetStandard12Runtimes;
                case TargetFramework.NetStandard13: return NetStandard13Runtimes;
                case TargetFramework.NetStandard14: return NetStandard14Runtimes;
                case TargetFramework.NetStandard15: return NetStandard15Runtimes;
                case TargetFramework.NetStandard16: return NetStandard16Runtimes;
                case TargetFramework.NetStandard20: return NetStandard20Runtimes;
                case TargetFramework.NetCoreApp11: return NetCoreApp11Runtimes;
                case TargetFramework.NetCoreApp20: return NetCoreApp20Runtimes;
                case TargetFramework.NetCoreApp21: return NetCoreApp21Runtimes;
                default: return Array.Empty<TargetRuntime>();
            }
        }

        /// <summary>
        /// Gets the <see cref="TargetRuntime"/> is a perfect match to this <see cref="TargetFramework"/>.
        /// NetStandard frameworks have no perfect runtime, but for <see cref="TargetFramework"/> that
        /// actually are <see cref="TargetRuntime"/> this returns the associated TargetRuntime enum value.
        /// </summary>
        /// <param name="this">This framework.</param>
        /// <returns>The single perfect runtime or <see cref="TargetRuntime.None"/>.</returns>
        static public TargetRuntime GetSinglePerfectRuntime( this TargetFramework @this )
        {
            switch( @this )
            {
                case TargetFramework.Net451:
                case TargetFramework.Net46:
                case TargetFramework.Net461: return TargetRuntime.Net461;
                case TargetFramework.Net462: return TargetRuntime.Net462;
                case TargetFramework.Net47: return TargetRuntime.Net47;
                case TargetFramework.Net471: return TargetRuntime.Net471;
                case TargetFramework.Net472: return TargetRuntime.Net472;
                case TargetFramework.NetStandard20: return TargetRuntime.None;
                case TargetFramework.NetCoreApp11: return TargetRuntime.NetCoreApp11;
                case TargetFramework.NetCoreApp20: return TargetRuntime.NetCoreApp20;
                case TargetFramework.NetCoreApp21: return TargetRuntime.NetCoreApp21;
                default: return TargetRuntime.None; 
            }
        }

        /// <summary>
        /// Gets whether a <see cref="TargetFramework"/> is compatible with a <see cref="TargetRuntime"/>.
        /// </summary>
        /// <param name="this">This framework.</param>
        /// <param name="r">The runtime.</param>
        /// <returns>True if the framework can run on the given runtime.</returns>
        static public bool CanWorkOn( this TargetFramework @this, TargetRuntime r )
        {
            return GetAllowedRuntimes( @this ).Contains( r );
        }

        /// <summary>
        /// Gets whether this <see cref="TargetFramework"/> is a .NetFramework.
        /// </summary>
        /// <param name="this">This framework.</param>
        /// <returns>True if this is a .Net framework.</returns>
        static public bool IsNetFramework( this TargetFramework @this )
        {
            return (((int)@this >> 16) & 127) > 0;
        }

        /// <summary>
        /// Parses the TargetFrameworkAttribute (from System.Runtime.Versioning).
        /// </summary>
        /// <param name="rawTargetFramework">
        /// String to parse (like ".NETFramework,Version=v4.7", ".NETStandard,Version=v1.6", or ".NETCoreApp,Version=v2.0"").</param>
        /// <returns>The target framework or <see cref="TargetFramework.None"/> if it is not known.</returns>
        static public TargetFramework TryParse( string rawTargetFramework )
        {
            switch( rawTargetFramework )
            {
                case ".NETFramework,Version=v4.5.1": return TargetFramework.Net451;
                case ".NETFramework,Version=v4.6.1": return TargetFramework.Net461;
                case ".NETFramework,Version=v4.6.2": return TargetFramework.Net462;
                case ".NETFramework,Version=v4.7": return TargetFramework.Net47;
                case ".NETStandard,Version=v1.0": return TargetFramework.NetStandard10;
                case ".NETStandard,Version=v1.1": return TargetFramework.NetStandard11;
                case ".NETStandard,Version=v1.2": return TargetFramework.NetStandard12;
                case ".NETStandard,Version=v1.3": return TargetFramework.NetStandard13;
                case ".NETStandard,Version=v1.4": return TargetFramework.NetStandard14;
                case ".NETStandard,Version=v1.5": return TargetFramework.NetStandard15;
                case ".NETStandard,Version=v1.6": return TargetFramework.NetStandard16;
                case ".NETStandard,Version=v2.0": return TargetFramework.NetStandard20;
                case ".NETCoreApp,Version=v1.0": return TargetFramework.NetCoreApp10;
                case ".NETCoreApp,Version=v1.1": return TargetFramework.NetCoreApp11;
                case ".NETCoreApp,Version=v2.0": return TargetFramework.NetCoreApp20;
                case ".NETCoreApp,Version=v2.1": return TargetFramework.NetCoreApp21;
            }
            return TargetFramework.None;
        }


    }
}
