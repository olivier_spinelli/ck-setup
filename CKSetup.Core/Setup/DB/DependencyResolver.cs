using CK.Core;
using CK.Text;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup
{
    /// <summary>
    /// Encapsulates resolution process.
    /// </summary>
    public class DependencyResolver
    {
        readonly DependencyEngine _engine;
        ComponentDB _db;

        internal DependencyResolver(
            ComponentDB db,
            TargetRuntime t,
            IEnumerable<ComponentDependency> roots,
            IEnumerable<BinFolder> targets,
            IReadOnlyList<BinFileAssemblyInfo> models,
            IReadOnlyList<BinFileAssemblyInfo> modelDependents )
        {
            Debug.Assert( db != null );
            Debug.Assert( targets != null && targets.Any() );
            Debug.Assert( models != null &&  (t == TargetRuntime.None || models.Count > 0) );
            Debug.Assert( modelDependents != null );
            _db = db;
            _engine = new DependencyEngine( db, t );
            TargetRuntime = t;
            Roots = roots.ToArray();
            Targets = targets.ToArray();
            Models = models;
            ModelDependents = modelDependents;
        }

        /// <summary>
        /// Gets whether this resolver has nothing to resolve.
        /// </summary>
        public bool IsEmpty => Roots.Count == 0;

        /// <summary>
        /// Gets the target runtime for which all components must be resolved.
        /// </summary>
        public TargetRuntime TargetRuntime { get; }

        /// <summary>
        /// Gets the list of <see cref="BinFolder"/>.
        /// </summary>
        public IReadOnlyList<BinFolder> Targets { get; }

        /// <summary>
        /// Gets the list of the initial dependencies that must be recursively resolved.
        /// </summary>
        public IReadOnlyList<ComponentDependency> Roots { get; }

        /// <summary>
        /// Gets the Models found among the <see cref="Targets"/>.
        /// </summary>
        public IReadOnlyList<BinFileAssemblyInfo> Models { get; }

        /// <summary>
        /// Gets the ModelDependents assemblies found among the <see cref="Targets"/>.
        /// </summary>
        public IReadOnlyList<BinFileAssemblyInfo> ModelDependents { get; }

        /// <summary>
        /// Encapsulates <see cref="Run"/> result.
        /// </summary>
        public struct RunResult
        {
            /// <summary>
            /// Gets the <see cref="DependencyResolver"/> that created this result.
            /// Never null.
            /// </summary>
            public readonly DependencyResolver Origin;

            /// <summary>
            /// The final component database, potentially updated with
            /// imported components.
            /// Null if success is false.
            /// </summary>
            public readonly ComponentDB FinalDB;

            /// <summary>
            /// The final resolved components.
            /// Null if success is false.
            /// </summary>
            public readonly IReadOnlyList<Component> Components;

            /// <summary>
            /// Gets whether the resoltion succeeded.
            /// </summary>
            public bool Success => Components != null;

            internal RunResult( DependencyResolver origin, ComponentDB db, IReadOnlyList<Component> components )
            {
                Debug.Assert( origin != null );
                Origin = origin;
                FinalDB = db;
                Components = components;
            }
        }

        /// <summary>
        /// Runs this resolver.
        /// </summary>
        /// <param name="monitor">The monitor.</param>
        /// <param name="downloader">Optional downloader of missing components.</param>
        /// <returns>Resolved dependencies on success, null on error.</returns>
        public RunResult Run( IActivityMonitor monitor, IComponentDownloader downloader )
        {
            using( monitor.OpenInfo( "Initializing root dependencies." ) )
            {
                if( !_engine.Initialize( monitor, Roots )
                    || !UpdateDBIfNeeded( monitor, downloader ) ) return new RunResult( this, null, null );
            }
            using( monitor.OpenInfo( "Resolving dependencies." ) )
            {
                while( !_engine.ExpandDependencies( monitor ) )
                {
                    if( !UpdateDBIfNeeded( monitor, downloader ) ) return new RunResult( this, null, null );
                }
            }
            return new RunResult( this, _db, _engine.Resolved );
        }

        bool UpdateDBIfNeeded( IActivityMonitor monitor, IComponentDownloader downloader )
        {
            if( _engine.HasMissing )
            {
                if( downloader != null )
                {
                    using( monitor.OpenInfo( $"Using donwloader." ) )
                    {
                        try
                        {
                            var missing = new ComponentMissingDescription( TargetRuntime, _engine.MissingDependencies, _engine.MissingEmbedded );
                            monitor.Info( missing.ToXml().ToString() );
                            var newDb = downloader.Download( monitor, missing );
                            if( newDb == null ) return false;
                            if( _db != newDb )
                            {
                                _db = newDb;
                                return _engine.OnDatabaseUpdated( monitor, newDb );
                            }
                        }
                        catch( Exception ex )
                        {
                            monitor.Error( ex );
                            return false;
                        }
                    }
                }
                if( _engine.MissingEmbedded.Count > 0 )
                {
                    monitor.Error( $"Missing embeddeds: {_engine.MissingEmbedded.Select( d => d.ToString() ).Concatenate() }." );
                }
                if( _engine.MissingDependencies.Count > 0 )
                {
                    monitor.Error( $"Missing required dependencies: {_engine.MissingDependencies.Select( d => d.ToString() ).Concatenate() }." );
                }
                return false;
            }
            return true;
        }
    }
}
