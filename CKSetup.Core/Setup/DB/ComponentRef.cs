using CK.Core;
using CSemVer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// Immutable struct that identifies a <see cref="Component"/>.
    /// </summary>
    public struct ComponentRef : IEquatable<ComponentRef>
    {
        readonly TargetFramework _targetFramework;
        readonly string _name;
        readonly SVersion _version;

        /// <summary>
        /// Initializes a new <see cref="ComponentRef"/>.
        /// </summary>
        /// <param name="n">Component name.</param>
        /// <param name="t">Target framework of the component.</param>
        /// <param name="v">Version of the component.</param>
        public ComponentRef( string n, TargetFramework t, SVersion v )
        {
            _targetFramework = t;
            _name = n;
            _version = v;
            CheckValid();
        }

        /// <summary>
        /// Initializes a new <see cref="ComponentRef"/> from its xml representation.
        /// </summary>
        /// <param name="e">The xml element. Must not be null.</param>
        public ComponentRef( XElement e )
        {
            _targetFramework = e.AttributeEnum( DBXmlNames.TargetFramework, TargetFramework.None );
            _name = (string)e.AttributeRequired( DBXmlNames.Name );
            _version = SVersion.Parse( (string)e.AttributeRequired( DBXmlNames.Version ) );
            CheckValid();
        }

        void CheckValid()
        {
            if( _targetFramework == TargetFramework.None ) throw new ArgumentException( "Invalid TargetFramework." );
            if( string.IsNullOrWhiteSpace( _name ) ) throw new ArgumentException( "Invalid Name." );
            if( _version == null || !_version.IsValid ) throw new ArgumentException( "Invalid Version." );
        }

        /// <summary>
        /// Gets the component's framework.
        /// </summary>
        public TargetFramework TargetFramework => _targetFramework;

        /// <summary>
        /// Gets the component's name.
        /// </summary>
        public string Name => _name;

        /// <summary>
        /// Returns a <see cref="ComponentRef"/> with the same name and version but a different framework.
        /// </summary>
        public ComponentRef WithTargetFramework( TargetFramework t ) => _targetFramework != t ? new ComponentRef( _name, t, _version ) : this;

        /// <summary>
        /// Gets the component's version.
        /// </summary>
        public SVersion Version => _version;

        /// <summary>
        /// Return the xml representation of this <see cref="ComponentRef"/>.
        /// </summary>
        /// <returns>The xml element.</returns>
        public XElement ToXml() => new XElement( DBXmlNames.Ref, XmlContent() );

        internal IEnumerable<XObject> XmlContent()
        {
            yield return new XAttribute( DBXmlNames.TargetFramework, _targetFramework );
            yield return new XAttribute( DBXmlNames.Name, _name );
            yield return new XAttribute( DBXmlNames.Version, _version.NormalizedText );
        }

        /// <summary>
        /// Gets the entry path prefix: "{Name}/{Version.NormalizedText}/{TargetFramework}/" (ends with a /).
        /// </summary>
        public string EntryPathPrefix => $"{Name}/{Version.NormalizedText}/{TargetFramework}/";

        /// <summary>
        /// Overridden to return the <see cref="EntryPathPrefix"/>.
        /// </summary>
        /// <returns></returns>
        public override string ToString() => EntryPathPrefix;

        /// <summary>
        /// Implement value semantics.
        /// </summary>
        /// <param name="other">The other reference.</param>
        /// <returns>True when this references the same component as the other one.</returns>
        public bool Equals( ComponentRef other ) => _targetFramework == other._targetFramework && _name == other._name && _version == other._version;

        /// <summary>
        /// Overridden to implement a value semantics.
        /// </summary>
        /// <param name="obj">The other potential reference.</param>
        /// <returns>True if the other object is a reference to the same component.</returns>
        public override bool Equals( object obj ) => obj is ComponentRef ? Equals( (ComponentRef)obj ) : false;

        /// <summary>
        /// Returns the hash code based on <see cref="TargetFramework"/>, <see cref="Name"/> and <see cref="Version"/>.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode() => Util.Hash.Combine( (long)_targetFramework, _name, _version ).GetHashCode();
    }
}
