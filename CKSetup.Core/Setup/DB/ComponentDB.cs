using CK.Core;
using CK.Text;
using CSemVer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// Immutable collection of <see cref="Component"/>.
    /// </summary>
    public class ComponentDB
    {
        /// <summary>
        /// The empty component database singleton.
        /// </summary>
        public static readonly ComponentDB Empty = new ComponentDB();

        /// <summary>
        /// Initializes a new empty <see cref="ComponentDB"/>.
        /// </summary>
        ComponentDB()
        {
            Components = Array.Empty<Component>();
        }

        /// <summary>
        /// Initializes a new <see cref="ComponentDB"/> from its <see cref="XElement"/> representation.
        /// </summary>
        /// <param name="e">The xml element.</param>
        public ComponentDB( XElement e )
        {
            var p = (string)e.Attribute( DBXmlNames.PrototypeStoreUrl );
            PrototypeStoreUrl = p != null ? new Uri( p ) : null;
            var comps = new List<Component>();
            Components = comps;
            foreach( var c in e.Elements( DBXmlNames.Component ) )
            {
                comps.Add( new Component( c ) );
            }
            Version = (long)e.Attribute( DBXmlNames.Version );
        }

        internal ComponentDB( ComponentDB origin, IEnumerable<Component> components, Uri protoType )
        {
            Components = components.ToArray();
            PrototypeStoreUrl = protoType;
            Version = origin.Version + 1;
        }

        ComponentDB( ComponentDB origin, Component[] components )
        {
            Components = components;
            PrototypeStoreUrl = origin.PrototypeStoreUrl;
            Version = origin.Version + 1;
        }

        /// <summary>
        /// Creates a <see cref="XElement"/> representation of this database.
        /// </summary>
        /// <returns>The Xml element.</returns>
        public XElement ToXml()
        {
            return new XElement( DBXmlNames.DB,
                                    PrototypeStoreUrl != null
                                        ? new XAttribute( DBXmlNames.PrototypeStoreUrl, PrototypeStoreUrl )
                                        : null,
                                    new XAttribute( DBXmlNames.Version, Version ),
                                    Components.Select( c => c.ToXml() ) );
        }

        /// <summary>
        /// Gets the version number of this database.
        /// </summary>
        public long Version { get; }

        /// <summary>
        /// Gets the prototype store url. Null if not specified.
        /// When specified, this store is used a fallback to resolve missing components and files.
        /// </summary>
        public Uri PrototypeStoreUrl { get; }

        /// <summary>
        /// Sets the <see cref="PrototypeStoreUrl"/>.
        /// </summary>
        /// <param name="url">The url to set. Can be null.</param>
        /// <returns>The new component database.</returns>
        public ComponentDB WithPrototypeStoreUrl( Uri url )
        {
            return new ComponentDB( this, Components is Component[] a ? a : Components.ToArray(), url );
        }

        /// <summary>
        /// Gets the list of registered components.
        /// </summary>
        public IReadOnlyList<Component> Components { get; }

        /// <summary>
        /// Gets the <see cref="ComponentRef"/> that should be added since they are
        /// currently discovered as embedded inside other ones.
        /// </summary>
        public IEnumerable<ComponentRef> EmbeddedComponents => Components.SelectMany( c => c.Embedded ).Distinct();

        /// <summary>
        /// Tries to finds the best available component for the given runtime, and a 
        /// minimal version.
        /// This method elects the lowest available version first with the highest target
        /// framework for this version that is allowed on the runtime.
        /// </summary>
        /// <param name="runtime">The runtime to consider.</param>
        /// <param name="name">The component name.</param>
        /// <param name="minVersion">Optional minimal version to satisfy.</param>
        /// <returns>The Component or null if not found.</returns>
        public Component FindBest( TargetRuntime runtime, string name, SVersion minVersion )
        {
            return Components
                    .Where( c => c.TargetFramework.CanWorkOn( runtime )
                                    && c.Name == name
                                    && c.Version >= minVersion )
                    .OrderBy( c => c.Version )
                    .ThenByDescending( c => c.TargetFramework )
                    .FirstOrDefault();
        }

        /// <summary>
        /// Finds a <see cref="ComponentRef"/>, returning null if not found.
        /// </summary>
        /// <param name="r">The component reference to find.</param>
        /// <returns>The registered component or null.</returns>
        public Component Find( ComponentRef r ) => Components.FirstOrDefault( c => c.Is( r ) );

        /// <summary>
        /// Finds a <see cref="ComponentRef"/> or throws a <see cref="InvalidOperationException"/>
        /// stating that the component is not registered.
        /// </summary>
        /// <param name="r">The component reference to find.</param>
        /// <returns>The registered component.</returns>
        public Component FindRequired( ComponentRef r )
        {
            Component c = Find( r );
            if( c == null ) throw new InvalidOperationException( $"Component '{r}' is not registered." );
            return c;
        }

        /// <summary>
        /// Returns a database from which some components are removed from this one.
        /// This does not remove any potential dependencies (a <see cref="ComponentDependency"/>
        /// only defines the name of its target and a minimal version).
        /// </summary>
        /// <param name="m">The monitor to use.</param>
        /// <param name="exclude">A predicate that must return true to remove the component.</param>
        /// <returns>A new component database or this one if no components have been removed.</returns>
        public ComponentDB RemoveComponents( IActivityMonitor m, Func<Component, bool> exclude )
        {
            using( m.OpenInfo( $"Removing components." ) )
            {
                var comps = Components.Where( c => !exclude( c ) ).ToArray();
                var result = comps.Length == Components.Count ? this : new ComponentDB( this, comps );
                if( result != this ) m.CloseGroup( $"{Components.Count - comps.Length} component(s) removed." );
                else m.CloseGroup( "No component has been removed." );
                return result;
            }
        }

        /// <summary>
        /// Captures the result of <see cref="ComponentDB.AddLocal"/>.
        /// </summary>
        public struct AddLocalResult
        {
            /// <summary>
            /// The new db. Null on error.
            /// </summary>
            public readonly ComponentDB NewDB;
            /// <summary>
            /// The added component. Can be null if the added component
            /// was already registered.
            /// </summary>
            public readonly Component NewComponent;
            /// <summary>
            /// True on error, otherwise false.
            /// </summary>
            public bool Error => NewDB == null;

            internal AddLocalResult( ComponentDB db, Component c = null )
            {
                NewDB = db;
                NewComponent = c;
            }
        }

        /// <summary>
        /// Registers a bin folder. 
        /// </summary>
        /// <param name="m">The monitor to use.</param>
        /// <param name="folder">The folder to register.</param>
        /// <returns>The new ComponentDB with added component.</returns>
        public AddLocalResult AddLocal( IActivityMonitor m, BinFolder folder )
        {
            if( !folder.Heads.Any() )
            {
                m.Error( "No components found." );
                return new AddLocalResult( null );
            }
            var freeHeads = folder.Heads.Where( h => Find( h.ComponentRef ) == null );
            int freeHeadsCount = freeHeads.Count();
            if( freeHeadsCount > 1 )
            {
                m.Error( $"Cannot register '{freeHeads.Select( h => h.Name.Name + " (" + h.LocalFileName + ')' ).Concatenate( "', '" )}' at the same time. They must be registered individually." );
                return new AddLocalResult( null );
            }
            if( freeHeadsCount == 0 )
            {
                m.Warn( $"No component added (found already registered Components: '{folder.Heads.Select( h => h.Name.Name + '/' + h.InfoVersion.NuGetVersion ).Concatenate( "', '" )}')" );
                return new AddLocalResult( this );
            }
            BinFileAssemblyInfo toAdd = freeHeads.Single();
            using( m.OpenInfo( $"Found '{toAdd.ComponentRef.EntryPathPrefix}' to register." ) )
            {
                List<ComponentDependency> dependencies = CollectSetupDependencies( m, toAdd.SetupDependencies );

                var embeddedComponents = new List<ComponentRef>();
                IEnumerable<BinFileInfo> binFiles = folder.Files;
                foreach( var sub in folder.Components )
                {
                    if( sub == toAdd ) continue;
                    var cSub = Find( sub.ComponentRef );
                    if( cSub != null )
                    {
                        if( dependencies.Any( d => d.UseName == cSub.Name ) )
                        {
                            m.Error( $"{cSub.Name} is declared as a Setup dependency but exists as an embedded component. Since project {toAdd.Name.Name} references {cSub}, it must not also declare a Setup Dependency on it." );
                            return new AddLocalResult( null );
                        }
                        if( toAdd.StoreFiles && cSub.StoreFiles )
                        {
                            dependencies.Add( new ComponentDependency( cSub.Name, cSub.Version ) );
                        }
                        m.Info( $"Removing {cSub.Files.Count} files thanks to already registered '{cSub.GetRef()}'." );
                        binFiles = binFiles.Where( f => !cSub.Files.Any( fc => fc.Name == f.LocalFileName ) );
                    }
                    else
                    {
                        m.Warn( $"Embedded component '{sub.ComponentRef}' will be included. It should be registered individually." );
                        embeddedComponents.Add( sub.ComponentRef );
                    }
                }
                var files = binFiles.Select( bf => new ComponentFile(
                                                    bf.LocalFileName,
                                                    bf.FileLength,
                                                    bf.ContentSHA1,
                                                    bf.FileVersion,
                                                    bf.AssemblyVersion,
                                                    bf is BinFileAssemblyInfo bfA
                                                        ? bfA.InfoVersion?.NuGetVersion
                                                        : null ) );
                var newC = new Component( toAdd.ComponentKind, toAdd.ComponentRef, dependencies, embeddedComponents, files );
                return new AddLocalResult( DoAdd( m, newC ), newC );
            }
        }

        ComponentDB DoAdd( IActivityMonitor m, Component newC )
        {
            return new ComponentDB( this, Components.Select( c => c.WithNewComponent( m, newC ) ).Append( newC ), PrototypeStoreUrl );
        }

        /// <summary>
        /// Exports a filtered set of components to a <see cref="Stream"/>.
        /// </summary>
        /// <param name="filter">Filter for components to export.</param>
        /// <param name="output">Output stream.</param>
        public void Export( Func<Component, bool> filter, Stream output )
        {
            using( CKBinaryWriter writer = new CKBinaryWriter( output, Encoding.UTF8, true ) )
            {
                // Version is currently 0.
                writer.WriteNonNegativeSmallInt32( 0 );
                foreach( var c in Components )
                {
                    if( filter( c ) )
                    {
                        writer.Write( true );
                        writer.Write( c.ToXml().ToString( SaveOptions.DisableFormatting ) );
                    }
                }
                writer.Write( false );
                writer.Flush();
            }
        }

        /// <summary>
        /// Captures the result of an import.
        /// </summary>
        public struct ImportResult
        {
            /// <summary>
            /// The new database with the imported components.
            /// </summary>
            public readonly ComponentDB NewDB;

            /// <summary>
            /// The imported components (whether they ar new or not).
            /// </summary>
            public readonly IReadOnlyList<Component> Components;

            /// <summary>
            /// Gets whether an error occurred (<see cref="NewDB"/> is null).
            /// </summary>
            public bool Error => NewDB == null;

            /// <summary>
            /// Initializes a new <see cref="ImportResult"/>.
            /// </summary>
            /// <param name="db">The resulting component database.</param>
            /// <param name="n">The imported components.</param>
            public ImportResult( ComponentDB db, IReadOnlyList<Component> n = null )
            {
                NewDB = db;
                Components = n;
            }
        }

        /// <summary>
        /// Imports a set of components from a <see cref="Stream"/>.
        /// </summary>
        /// <param name="monitor">Monitor to use.</param>
        /// <param name="input">Input stream.</param>
        /// <returns>The new ComponentDB with imported components.</returns>
        public ImportResult Import( IActivityMonitor monitor, Stream input )
        {
            using( monitor.OpenInfo( "Starting components import." ) )
            using( CKBinaryReader reader = new CKBinaryReader( input, Encoding.UTF8, true ) )
            {
                var newOnes = new List<ComponentRef>();
                ComponentDB currentDb = this;
                try
                {
                    var v = reader.ReadNonNegativeSmallInt32();
                    monitor.Debug( $"Stream version: {v}" );
                    while( reader.ReadBoolean() )
                    {
                        var newC = new Component( XElement.Parse( reader.ReadString() ) );
                        bool skip = currentDb.Find( newC.GetRef() ) != null;
                        if( skip )
                        {
                            monitor.Warn( $"Skipping '{newC}' since it already exists." );
                        }
                        else
                        {
                            monitor.Trace( $"Importing Component '{newC}' ({newC.Files.Count} files)." );
                            currentDb = currentDb.DoAdd( monitor, newC );
                        }
                        newOnes.Add( newC.GetRef() );
                    }
                }
                catch( Exception ex )
                {
                    monitor.Error( "Stream obtained from the remote is invalid.", ex );
                    return new ImportResult( null );
                }
                return new ImportResult( currentDb, newOnes.Select( n => currentDb.Components.Single( c => c.GetRef().Equals( n ) ) ).ToList() );
            }
        }

        /// <summary>
        /// Captures result of <see cref="FindAvailable( IActivityMonitor,ComponentMissingDescription)"/> local lookup.
        /// </summary>
        public struct LocallyAvailable
        {
            /// <summary>
            /// Resolved components (either from <see cref="ComponentMissingDescription.Components"/> or <see cref="ComponentMissingDescription.Dependencies"/>.
            /// </summary>
            public readonly ISet<Component> Resolved;

            /// <summary>
            /// Unresolved component from <see cref="ComponentMissingDescription.Components"/>.
            /// </summary>
            public readonly IReadOnlyList<ComponentRef> Unresolved;

            /// <summary>
            /// Dependencies from <see cref="ComponentMissingDescription.Dependencies"/> that are missing or
            /// resolved to a greater version than the <see cref="ComponentDependency.UseMinVersion"/>.
            /// </summary>
            public readonly IReadOnlyList<ComponentDependency> NotPerfectDependencies;

            /// <summary>
            /// Gets whether at least one <see cref="Unresolved"/> or <see cref="NotPerfectDependencies"/> exists.
            /// </summary>
            public bool ShouldBeCompleted => Unresolved.Count > 0 || NotPerfectDependencies.Count > 0;

            internal LocallyAvailable( ISet<Component> r, ISet<ComponentRef> u, ISet<ComponentDependency> d )
            {
                Resolved = r;
                Unresolved = u.ToList();
                NotPerfectDependencies = d.ToList();
            }
        }

        /// <summary>
        /// Gets a list of available components. Missing components (<see cref="ComponentMissingDescription.Components"/>)
        /// are looked up exactly and dependencies (<see cref="ComponentMissingDescription.Dependencies"/>) uses <see cref="FindBest(TargetRuntime, string, SVersion)"/>
        /// method.
        /// </summary>
        /// <param name="monitor">Monitor to use.</param>
        /// <param name="what">Required description.</param>
        /// <returns>Available components (can be empty).</returns>
        public LocallyAvailable FindAvailable( IActivityMonitor monitor, ComponentMissingDescription what )
        {
            var resolved = new HashSet<Component>();
            var unresolved = new HashSet<ComponentRef>();
            var notPerfect = new HashSet<ComponentDependency>();
            using( monitor.OpenInfo( $"Finding locally available for {what.TargetRuntime}, {what.Components.Count} components and {what.Dependencies.Count} dependencies." ) )
            {
                if( what.Components.Count > 0 )
                {
                    foreach( var cRef in what.Components )
                    {
                        Component c = Components.FirstOrDefault( x => x.GetRef().Equals( cRef ) );
                        if( c != null && c.ComponentKind != ComponentKind.None )
                        {
                            monitor.Warn( $"Found: {cRef} component." );
                            resolved.Add( c );
                        }
                        else
                        {
                            monitor.Warn( $"Not found: {cRef} component." );
                            unresolved.Add( cRef );
                        }
                    }
                }
                if( what.Dependencies.Count > 0 )
                {
                    using( monitor.OpenInfo( "Resolving dependencies." ) )
                    {
                        var depResolved = new List<Component>();
                        foreach( var dep in what.Dependencies )
                        {
                            var c = FindBest( what.TargetRuntime, dep.UseName, dep.UseMinVersion );
                            if( c != null )
                            {
                                if( resolved.Add( c ) )
                                {
                                    depResolved.Add( c );
                                    if( dep.UseMinVersion != null && c.Version != dep.UseMinVersion )
                                    {
                                        monitor?.Warn( $"Found: {dep} dependency but with version {c.Version}." );
                                        notPerfect.Add( dep );
                                    }
                                    else
                                    {
                                        monitor.Info( $"Found: {dep}: version {c.Version}." );
                                    }
                                }
                                else monitor.Info( $"Found: {dep} dependency (already found as a requested Component)." );
                            }
                            else
                            {
                                monitor.Warn( $"Not found: {dep} dependency." );
                                notPerfect.Add( dep );
                            }
                        }
                    }
                }
            }
            return new LocallyAvailable( resolved, unresolved, notPerfect );
        }

        /// <summary>
        /// Returns a new <see cref="DependencyResolver"/> for a set of <see cref="BinFolder"/> targets.
        /// </summary>
        /// <param name="m">The monitor to use.</param>
        /// <param name="targets">The targets for which required components must be found.</param>
        /// <param name="explicitDependencies">Optional extra dependencies that, when specified, must be resolved.</param>
        /// <param name="preferredTargetRuntimes">
        /// Optional ordered set of <see cref="TargetRuntime"/> that will be selected
        /// if possible. First is better.
        /// </param>
        /// <returns>Null on error, otherwise the DependencyResolver (may be empty).</returns>
        public DependencyResolver GetRuntimeDependenciesResolver(
            IActivityMonitor m,
            IEnumerable<BinFolder> targets,
            IEnumerable<SetupDependency> explicitDependencies = null,
            IEnumerable<TargetRuntime> preferredTargetRuntimes = null )
        {
            if( targets == null ) throw new ArgumentNullException( nameof( targets ) );
            using( m.OpenInfo( $"Creating runtime dependencies resolver for {targets.Select( t => t.BinPath ).Concatenate()}." ) )
            {
                var models = targets.SelectMany( t => t.Components )
                                    .Where( c => c.ComponentKind == ComponentKind.Model )
                                    .ToArray();
                if( models.Length == 0 )
                {
                    m.Warn( "No Model component found." );
                    return new DependencyResolver( this, TargetRuntime.None, Array.Empty<ComponentDependency>(), targets, Array.Empty<BinFileAssemblyInfo>(), Array.Empty<BinFileAssemblyInfo>() );
                }
                foreach( var eOrR in targets.SelectMany( t => t.Components ).Where( c => c.ComponentKind != ComponentKind.Model ) )
                {
                    m.Warn( $"Found a SetupDependency '{eOrR.ComponentRef}' component. It will be ignored: only Models are considered when selecting TargetRuntime." );
                }

                m.Info( $"Found Models: {models.Select( x => x.ComponentRef.ToString() ).Concatenate() }" );

                var modelDependents = targets.SelectMany( b => b.Assemblies )
                                             .Where( a => a.ComponentKind == ComponentKind.None
                                                          && a.LocalDependencies.Any( dep => dep.ComponentKind == ComponentKind.Model ) )
                                             .Distinct()
                                             .ToArray();
                m.Info( $"Found ModelDependents: {modelDependents.Select( x => $"{x.Name.Name} ({x.TargetFramework})" ).Concatenate() }" );

                var targetRuntime = SelectTargetRuntime( m, models, modelDependents, preferredTargetRuntimes );
                if( targetRuntime == TargetRuntime.None ) return null;

                var allDeps = models.SelectMany( b => b.SetupDependencies );
                if( explicitDependencies != null ) allDeps = allDeps.Concat( explicitDependencies );
                var rootDeps = CollectSetupDependencies( m, allDeps );
                if( rootDeps.Count == 0 ) m.Warn( "No Setup Dependency components found." );
                return new DependencyResolver( this, targetRuntime,  rootDeps, targets, models, modelDependents );
            }
        }

        /// <summary>
        /// Resolves all dependencies from a root component.
        /// The first component (as long as the return is not null), is the root component.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="name">The component name.</param>
        /// <param name="runtime">The target runtime.</param>
        /// <param name="version">Optional version. When null, the greatest version will be returned.</param>
        /// <returns>The component list on success, null if the root component or its dependencies can not be resolved.</returns>
        public IReadOnlyList<Component> ResolveLocalDependencies( IActivityMonitor monitor, string name, TargetRuntime runtime, SVersion version = null )
        {
            Component root;
            if( version != null )
            {
                root = FindBest( runtime, name, version );
                if( root == null )
                {
                    monitor.Error( $"Unable to find component '{name}/{runtime}/{version.NormalizedText}'." );
                    return null;
                }
            }
            else
            {
                root = Components.Where( c => c.Name == name && c.TargetFramework.CanWorkOn( runtime ) )
                            .OrderByDescending( c => c.Version )
                            .OrderByDescending( c => c.TargetFramework )
                            .FirstOrDefault();
                if( root == null )
                {
                    monitor.Error( $"Unable to find component '{name}/{runtime}'." );
                    return null;
                }
            }
            return ExpandDependencies( monitor, root, runtime );
        }

        /// <summary>
        /// Resolves all dependencies from a root component.
        /// The first component (as long as the return is not null), is the root component.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="name">The component name.</param>
        /// <param name="runtime">The target runtime.</param>
        /// <param name="firstVersionMatcher">
        /// Optional first version matcher.
        /// When null, the greatest version will be returned.
        /// When not null, this predicate is called from the last (greatest) version to the lowest one: the
        /// first time it returns true will be selected with the highest framework allowed on the target runtime.
        /// </param>
        /// <returns>The component list on success, null if the root component or its dependencies can not be resolved.</returns>
        public IReadOnlyList<Component> ResolveLocalDependencies( IActivityMonitor monitor, string name, TargetRuntime runtime, Func<SVersion, bool> firstVersionMatcher = null )
        {
            Component root;
            if( firstVersionMatcher == null ) return ResolveLocalDependencies( monitor, name, runtime, (SVersion)null );
            root = Components.Where( c => c.Name == name && c.TargetFramework.CanWorkOn( runtime ) )
                        .OrderByDescending( c => c.Version )
                        .Where( c => firstVersionMatcher( c.Version ) )
                        .OrderByDescending( c => c.TargetFramework )
                        .FirstOrDefault();
            if( root == null )
            {
                monitor.Error( $"Unable to find component '{name}/{runtime}' that satisfies the version requirement." );
                return null;
            }
            return ExpandDependencies( monitor, root, runtime );
        }

        List<Component> ExpandDependencies(IActivityMonitor monitor, Component root, TargetRuntime runtime)
        {
            var engine = new DependencyEngine( this, runtime, root );
            var results = engine.ExpandDependencies( monitor ) ? engine.Resolved : null;
            Debug.Assert( results == null || results[0] == root );
            return results;
        }

        static TargetRuntime SelectTargetRuntime(
            IActivityMonitor m,
            IReadOnlyList<BinFileAssemblyInfo> models,
            IReadOnlyList<BinFileAssemblyInfo> modelDependents,
            IEnumerable<TargetRuntime> preferredTargetRuntimes )
        {
            Debug.Assert( models.Count > 0 );
            using( m.OpenInfo( $"Selecting TargetRuntime." ) )
            {
                var modelFrameworks = models.Skip( 1 ).Select( x => x.ComponentRef.TargetFramework );
                var modelDependentFrameworks = modelDependents
                                                .Select( x => x.TargetFramework )
                                                .Where( t => t != TargetFramework.None );
                var runtimes = models[0].ComponentRef.TargetFramework.GetCommonRuntimes( modelFrameworks.Concat( modelDependentFrameworks ) );
                if( !runtimes.Any() )
                {
                    m.Error( $"Unable to determine at least one common allowed runtime." );
                    return TargetRuntime.None;
                }
                var theOnlyOne = runtimes.Count() == 1 ? runtimes.First() : TargetRuntime.None;
                if( theOnlyOne != TargetRuntime.None )
                {
                    m.CloseGroup( $"Single selected runtime: {theOnlyOne}." );
                    return theOnlyOne;
                }
                m.Info( $"Multiple possible runtime: {runtimes.Select( r => r.ToString() ).Concatenate()}." );
                if( preferredTargetRuntimes != null && preferredTargetRuntimes.Any() )
                {
                    m.Info( $"Preferred target runtimes is: {preferredTargetRuntimes.Select( t => t.ToString() ).Concatenate()}." );
                    theOnlyOne = preferredTargetRuntimes.FirstOrDefault( r => runtimes.Contains( r ) );
                    if( theOnlyOne == TargetRuntime.None )
                    {
                        m.Warn( $"The possible runtimes can not be found in preferred target runtimes ({preferredTargetRuntimes.Select( t => t.ToString() ).Concatenate()}). Using the lowest one." );
                    }
                }
                else m.Info( "No preferred target runtimes specified. Using the lowest one." );
                if( theOnlyOne == TargetRuntime.None ) theOnlyOne = runtimes.Min();
                m.CloseGroup( $"Selected runtime: {theOnlyOne}." );
                return theOnlyOne;
            }
        }

        static List<ComponentDependency> CollectSetupDependencies( IActivityMonitor m, IEnumerable<SetupDependency> deps )
        {
            var dependencies = new List<ComponentDependency>();
            foreach( var dep in deps.GroupBy( d => d.UseName ) )
            {
                string name = dep.Key;
                var versions = dep.Where( d => d.UseMinVersion != null ).Select( d => d.UseMinVersion ).Distinct().ToList();
                if( versions.Count == 0 )
                {
                    dependencies.Add( new ComponentDependency( name, null ) );
                }
                else if( versions.Count == 1 )
                {
                    dependencies.Add( new ComponentDependency( name, versions[0] ) );
                }
                else
                {
                    var max = versions.Max();
                    var culprits = dep.Where( d => d.UseName == name );
                    using( m.OpenWarn( $"Version upgrade for '{name}'. Using: {max}." ) )
                    {
                        foreach( var c in culprits )
                        {
                            m.Warn( $"'{c.SourceName}' declares to use the version {c.UseMinVersion}." );
                        }
                    }
                    dependencies.Add( new ComponentDependency( name, max ) );
                }
            }
            return dependencies;
        }
    }
}
