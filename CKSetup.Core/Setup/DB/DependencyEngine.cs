using CK.Core;
using CK.Text;
using CSemVer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup
{
    class DependencyEngine
    {
        readonly TargetRuntime _target;
        // This contains fixed requirements of embedded ComponentRef.
        readonly HashSet<ComponentRef> _embeddeds;
        readonly List<ComponentDependency> _deps;
        readonly List<Component> _resolved;

        ComponentDB _db;
        
        internal DependencyEngine( ComponentDB db, TargetRuntime t, Component root = null )
        {
            _db = db;
            _target = t;
            _resolved = new List<Component>();
            _deps = new List<ComponentDependency>();
            _embeddeds = new HashSet<ComponentRef>();
            if( root != null ) _resolved.Add( root );
        }

        /// <summary>
        /// Gets resolved dependencies.
        /// </summary>
        public List<Component> Resolved => _resolved;

        /// <summary>
        /// Gets any missing dependencies that have been found.
        /// </summary>
        public IReadOnlyCollection<ComponentDependency> MissingDependencies => _deps;

        /// <summary>
        /// Gets any missing embedded references that have been found.
        /// </summary>
        public IReadOnlyCollection<ComponentRef> MissingEmbedded => _embeddeds;

        /// <summary>
        /// Gets whether a missing dependency or embedded has been found.
        /// </summary>
        public bool HasMissing => _embeddeds.Count > 0 || _deps.Count > 0;

        /// <summary>
        /// Gets the root dependencies.
        /// </summary>
        public IReadOnlyList<ComponentDependency> Roots { get; private set; }

        /// <summary>
        /// Initializes this engine with initial dependencies.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="roots">The intitial required dependencies.</param>
        /// <returns>Always true.</returns>
        internal bool Initialize( IActivityMonitor monitor, IReadOnlyList<ComponentDependency> roots )
        {
            Roots = roots;
            foreach( var d in roots ) Handle( monitor, d );
            return true;
        }

        public bool ExpandDependencies( IActivityMonitor monitor )
        {
            int idx = 0;
            do
            {
                foreach( var dep in _resolved[idx].Dependencies )
                {
                    int idxLocalUgraded = Handle( monitor, dep, _resolved[idx].Name );
                    if( HasMissing ) return false;
                    if( idxLocalUgraded >= 0 && idxLocalUgraded <= idx )
                    {
                        // An already existing resolved dependency has been upgraded
                        // and it has been (or is being) processed.
                        // We restart the process, keeping at least the root dependencies.
                        int succesfulIndex = Math.Max( idxLocalUgraded + 1, Roots.Count );
                        int countToRemove = _resolved.Count - succesfulIndex;
                        _resolved.RemoveRange( succesfulIndex, countToRemove );
                        idx = succesfulIndex;
                        monitor.Trace( $"Restarting from {succesfulIndex}, backtracking {countToRemove} previous resolutions." );
                    }
                }
            }
            while( ++idx < _resolved.Count );
            return true;
        }

        public bool OnDatabaseUpdated( IActivityMonitor monitor, ComponentDB db )
        {
            using( monitor.OpenInfo( "Updating DependencyEngine state." ) )
            {
                // Unifies and captures dependencies and embedded refs as dependencies in a list.
                var unified = _resolved.Select( r => new ComponentDependency( r.Name, r.Version ) )
                                    .Concat( _deps )
                                    .Concat( _embeddeds.Select( d => new ComponentDependency( d.Name, d.Version ) ) )
                                    .GroupBy( d => d.UseName )
                                    .Select( g => g.MaxBy( d => d.UseMinVersion ) )
                                    .ToList();
                // Clean current state.
                _resolved.Clear();
                _deps.Clear();
                _embeddeds.Clear();
                // Updates new state with the current resolved Components.
                int failedCount = 0;
                foreach( var d in unified )
                {
                    var c = db.FindBest( _target, d.UseName, d.UseMinVersion );
                    if( c == null )
                    {
                        monitor.Error( $"Failed to resolve dependency {d}." );
                        ++failedCount;
                    }
                    else _resolved.Add( c );
                }
                _db = db;
                monitor.Trace( $"Resolved is now: {_resolved.Select( d => d.ToString() ).Concatenate()}." );
                if( failedCount > 0 ) return false;
                // On every success, initial root dependencies are stable.
                Debug.Assert( _resolved.Take( Roots.Count ).Select( r => r.Name ).OrderBy( n => n )
                    .SequenceEqual( Roots.Select( r => r.UseName ).OrderBy( n => n ) ) );
                return true;
            }
        }

        /// <summary>
        /// Handles a dependency.
        /// </summary>
        /// <param name="monitor">The lmonitor to use.</param>
        /// <param name="d">The dependency to check.</param>
        /// <param name="sourceName">Optional source name.</param>
        int Handle( IActivityMonitor monitor, ComponentDependency d, string sourceName = "" )
        {
            int idxLocalUpgraded = FindLocal( monitor, d );
            if( idxLocalUpgraded == -2 )
            {
                int idx = _deps.FindIndex( x => x.UseName == d.UseName && x.UseMinVersion >= d.UseMinVersion );
                if( idx >= 0 )
                {
                    monitor.Debug( $"Already existing dependency {_deps[idx]} for {d}." );
                }
                else
                {
                    idx = _deps.FindIndex( x => x.UseName == d.UseName && x.UseMinVersion < d.UseMinVersion );
                    if( idx >= 0 )
                    {
                        monitor.Trace( $"Upgrading required dependency {sourceName}{d}." );
                        _deps[idx] = d;
                    }
                    else
                    {
                        monitor.Trace( $"Adding required dependency {sourceName}{d}." );
                        _deps.Add( d );
                    }
                }
            }
            return idxLocalUpgraded;
        }

        int FindLocal( IActivityMonitor monitor, ComponentDependency d )
        {
            int idx = _resolved.FindIndex( c => c.Name == d.UseName );
            if( idx < 0 )
            {
                var found = _db.FindBest( _target, d.UseName, d.UseMinVersion );
                if( found != null )
                {
                    var perfectRuntime = found.TargetFramework.GetSinglePerfectRuntime();
                    if( perfectRuntime != TargetRuntime.None && perfectRuntime != _target )
                    {
                        monitor.Warn( $"Required dependency {d} resolved to local {found}. This is not the perfect runtime ({_target})." );
                        return -2;
                    }
                    else monitor.Trace( $"Resolved required dependency {d} to local {found}." );
                    _resolved.Add( HandleEmbedded( monitor, found ) );
                    return -1;
                }
            }
            else
            {
                var exists = _resolved[idx];
                if( exists.Version >= d.UseMinVersion )
                {
                    monitor.Debug( $"Required dependency {d} resolved to existing local {exists}." );
                    return -1;
                }
                var found = _db.FindBest( _target, d.UseName, d.UseMinVersion );
                if( found != null )
                {
                    monitor.Trace( $"Local {exists} upgraded to local {found}." );
                    _resolved[idx] = HandleEmbedded( monitor, found );
                    return idx;
                }
            }
            return -2;
        }

        Component HandleEmbedded( IActivityMonitor monitor, Component found )
        {
            foreach( var m in found.Embedded )
            {
                if( _embeddeds.Add( m ) )
                {
                    int idx = _deps.FindIndex( x => x.UseName == m.Name && x.UseMinVersion <= m.Version );
                    if( idx >= 0 )
                    {
                        // The exact reference will satisfy the floating one.
                        monitor.Trace( $"Required dependency {_deps[idx]} upgraded to embedded {m}." );
                        _deps.RemoveAt( idx );
                    }
                }
            }
            return found;
        }

    }
}
