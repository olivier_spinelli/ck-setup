using CK.Core;
using CSemVer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// A component dependency is defined by a <see cref="UseName"/> component's name
    /// and its <see cref="UseMinVersion"/> minimal version.
    /// This is an immutable and equatable value (both name and minimal version must be equal).
    /// </summary>
    public class ComponentDependency : IEquatable<ComponentDependency>
    {
        internal ComponentDependency( string name, SVersion version )
        {
            Debug.Assert( !string.IsNullOrWhiteSpace( name ) );
            Debug.Assert( version == null || version.IsValid );
            UseName = name;
            UseMinVersion = version;
        }

        internal ComponentDependency( XElement e )
        {
            UseName = (string)e.AttributeRequired( DBXmlNames.Name );
            var sV = (string)e.Attribute( DBXmlNames.Version );
            if( sV != null ) UseMinVersion = SVersion.Parse( sV );
        }

        internal XElement ToXml()
        {
            return new XElement( DBXmlNames.Dependency, 
                                    new XAttribute( DBXmlNames.Name, UseName ),
                                    UseMinVersion != null 
                                        ? new XAttribute( DBXmlNames.Version, UseMinVersion ) 
                                        : null );
        }

        /// <summary>
        /// Gets the name of the referenced component.
        /// </summary>
        public string UseName { get; }

        /// <summary>
        /// Gets the minimal version. Can be null.
        /// </summary>
        public SVersion UseMinVersion { get; }

        /// <summary>
        /// Overridden to return a string with <see cref="UseName"/> and <see cref="UseMinVersion"/>.
        /// </summary>
        /// <returns>A readable string.</returns>
        public override string ToString() => $"-> {UseName}/{(UseMinVersion == null ? "<no min version>" : UseMinVersion.NormalizedText)}";

        /// <summary>
        /// Checks equality with another dependency.
        /// </summary>
        /// <param name="other">The other dependency.</param>
        /// <returns>True if they are equal, false otherwise.</returns>
        public bool Equals( ComponentDependency other ) => other != null && other.UseName == UseName && other.UseMinVersion == UseMinVersion;

        /// <summary>
        /// Gets the hash code.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode() => (UseMinVersion?.GetHashCode() ?? 0) ^ UseName.GetHashCode();

        /// <summary>
        /// Overridden to support value semantics.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if they are equal, false otherwise.</returns>
        public override bool Equals( object obj ) => Equals( obj as ComponentDependency );

    }
}
