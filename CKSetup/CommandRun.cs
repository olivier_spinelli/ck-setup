using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using CK.Core;
using Microsoft.Extensions.CommandLineUtils;
using CK.Text;
using Mono.Cecil;
using System.Linq;
using Mono.Collections.Generic;
using CK.Setup;
using System.Xml.Linq;
using System.Diagnostics;

namespace CKSetup
{
    /// <summary>
    /// Run command implementation.
    /// </summary>
    static public class CommandRun
    {
        /// <summary>
        /// Implements CKSetup run command.
        /// </summary>
        /// <param name="c">The application.</param>
        public static void Define( CommandLineApplication c )
        {
            c.FullName = c.Parent.FullName;
            c.Description = "Resolves Model/Setup dependencies accross one ore more directories and run a command in a consolidated folder.";
            c.StandardConfiguration( true );
            ConfigurationPathArgument pathArg = c.ConfigurationPathArgument();
            StorePathOptions storePath = c.AddStorePathOption();
            RemoteUriOptions remoteOpt = c.AddRemoteUriOptions();
            var debugOption = c.Option( "--launchDebug", $"Calls Debugger.Launch() at the very start of the runner process.", CommandOptionType.SingleValue );
            var forceSetupOption = c.Option( "--force", $"Runs the setup, regardless of the signature files that may exist in the configured BinPaths folders.", CommandOptionType.SingleValue );

            c.OnExecute( monitor =>
            {
                if( !pathArg.Initialize( monitor ) ) return Program.RetCodeError;
                if( !storePath.Initialize( monitor ) ) return Program.RetCodeError;
                if( !remoteOpt.Initialize( monitor ) ) return Program.RetCodeError;

                using( LocalStore store = LocalStore.OpenOrCreate( monitor, storePath.StorePath ) )
                {
                    if( store == null ) return Program.RetCodeError;

                    using( IRemoteStore remote = remoteOpt.Url != null && remoteOpt.Url != store.PrototypeStoreUrl
                                                ? ClientRemoteStore.Create( monitor, remoteOpt.Url, remoteOpt.ApiKey )
                                                : null )
                    {
                        return Facade.DoRun( monitor,
                                             store,
                                             pathArg.Configuration,
                                             remote,
                                             debugOption.HasValue(),
                                             forceSetupOption.HasValue() ) != CKSetupRunResult.Failed
                                ? Program.RetCodeSuccess
                                : Program.RetCodeError;
                    }
                }
            } );
        }
    }
}
